$(document).ready(function () {

    $('#company').change(function () {
        $('#company-input').val($('#company').val());
    });
    $('#car').change(function () {
        $('#car-input').val($('#car').val());
    });
    $('#car-model').change(function () {
        $('#car-model-input').val($('#car-model').val());
    });
    $('#product-name').change(function () {
        $('#product-name-input').val($('#product-name').val());
    });
    $('#product-company').change(function () {
        $('#product-company-input').val($('#product-company').val());
    });
    $('#company-input').keyup(function () {
        $('#company').val('');
    });
    $('#car-input').keyup(function () {
        $('#car').val('');
    });
    $('#car-model-input').keyup(function () {
        $('#car-model').val('');
    });
    $('#product-name-input').keyup(function () {
        $('#product-name').val('');
    });
    $('#product-company-input').keyup(function () {
        $('#product-company').val('');
    });


    $('#company').change(function () {
        var company_name = $('#company').val();
        $.ajax({
            url: "get-cars",
            type: "GET",
            dataType: "json",
            data: {
                name: company_name
            },
            success: function (response) {
                $('#car').children('option:not(:first)').remove();
                $.each(response.cars, function (key, value) {
                    $('#car')
                            .append($("<option></option>")
                                    .attr("value", value.name)
                                    .text(value.name));
                });
            },
            error: function (error) {
                console.log('not succ');
            }
        });
    });
    $('#car').change(function () {
        var car_name = $('#car').val();
        $.ajax({
            url: "get-models",
            type: "GET",
            dataType: "json",
            data: {
                name: car_name
            },
            success: function (response) {
                $('#car-model').children('option:not(:first)').remove();
                $.each(response.carModels, function (key, value) {
                    $('#car-model')
                            .append($("<option></option>")
                                    .attr("value", value.name)
                                    .text(value.model));
                });
            },
            error: function (error) {

            }
        });
    });
//    $('.changeDeliveryStatus').click(function (e) {
//        var $this = $(this);
//        $.ajax({
//            url: $this.attr('href'),
//            type: "GET",
//            dataType: "json",
//            success: function (response) {
//                $this.parents('.each-order').find('.delivery-status').text('Delivered');
//            },
//            error: function (error) {
//
//            }
//        });
//        e.preventDefault();
//    });



});