$(document).ready(function () {

//    $('.submit_cell').click(function () {
//        $('#search_block form').submit();
//        console.log('red');
//    });
//    $('.search_block form').on('submit', function(e) { 
//      console.log('red22');  
//    });
    $('#login').click(function () {
        if ($("#myLogin").hasClass("show")) {
            $('#myLogin').removeClass('show');
        } else {
            $('#recover-popup').removeClass('show');
            $('#myLogin').removeClass('show');
            $('#mySignup').removeClass('show');
            $('#myLogin').addClass("show");
        }
    })
    $('#signup').click(function () {
        if ($("#mySignup").hasClass("show")) {
            $('#mySignup').removeClass('show');
        } else {
            $('#recover-popup').removeClass('show');
            $('#myLogin').removeClass('show');
            $('#mySignup').removeClass('show');
            $('#mySignup').addClass("show");
        }
    })
    $('.recover_password').click(function () {
        if ($("#recover-popup").hasClass("show")) {
            $('#recover-popup').removeClass('show');
        } else {
            $('#myLogin').removeClass('show');
            $('#recover-popup').addClass("show");
        }
    });
    $(document).on('click', function (e) {
        if ($(e.target).closest("#signup").length === 0 && $(e.target).closest("#mySignup").length === 0) {
            $('#mySignup').removeClass('show');
        }
        if ($(e.target).closest("#login").length === 0 && $(e.target).closest("#myLogin").length === 0) {
            $('#myLogin').removeClass('show');
        }
        if ($(e.target).closest(".recover_password").length === 0 && $(e.target).closest("#recover-popup").length === 0) {
            $('#recover-popup').removeClass('show');
        }
    });



    $('#companies-name').change(function () {
        var company_id = $('#companies-name').val();
        $.ajax({
            url: "get-cars",
            type: "GET",
            dataType: "json",
            data: {
                id: company_id
            },
            success: function (response) {
                $('#company-cars').children('option:not(:first)').remove();
                $.each(response.cars, function (key, value) {
                    $('#company-cars')
                            .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.name));
                });
            },
            error: function (error) {
                console.log('not succ');
            }
        });
    });
    $('#company-cars').change(function () {
        var car_id = $('#company-cars').val();
        $.ajax({
            url: "get-models",
            type: "GET",
            dataType: "json",
            data: {
                id: car_id
            },
            success: function (response) {
                $('#car-models').children('option:not(:first)').remove();
                $.each(response.carModels, function (key, value) {
                    $('#car-models')
                            .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.model));
                });
            },
            error: function (error) {
               
            }
        });
    });
    $('#cities-name').change(function () {
        var state_id = $('#cities-name').find(':selected').attr('data-state_id');
        var delivery_charges = $('#deliveryCharges').val();
        $.ajax({
            url: "get-state",
            type: "GET",
            dataType: "json",
            data: {
                id: state_id
            },
            success: function (response) {
                $('#states-name').children('option:not(:first)').remove();
                $.each(response.state, function (key, value) {
                    $('#states-name')
                            .append($("<option></option>")
                                    .attr("value", value.id)
                                    .text(value.name));
                });
                $('#dilevery_charges').show();
                $('#dilevery_charges p').append(delivery_charges);
            },
            error: function (error) {
                
            }
        });
    });
    setTimeout(function () {
        $('.alert').fadeOut('fast');
    }, 2000);
    $('.spinner .btn:first-of-type').on('click', function () {
        $('.spinner input').val(parseInt($('.spinner input').val(), 10) + 1);
    });
    $('.spinner .btn:last-of-type').on('click', function () {
        if ($('.spinner input').val() == 0) {
        } else {
            $('.spinner input').val(parseInt($('.spinner input').val(), 10) - 1);
        }
    });
    $('#add-to-cart-items').change(function () {
        var val = $('#add-to-cart-items').val();
    });
    $('.change-qty-form select').on('change', function () {
        var $this = $(this),
                value = $this.val(),
                productPrice = $this.attr('data-product_price'),
                installationCharges = $this.attr('data-installation_charges');
        $this.parents('.cart-list-item').find('.price-tag').text(value * productPrice);
        $this.parents('.cart-list-item').find('.install-tag').text(value * installationCharges);
        //adding prices
        var tPrice = 0;
        $('.cart-list-item .price-tag').each(function () {
            tPrice = parseInt(tPrice) + parseInt($(this).text());
        });
        $('.cart-side-secure--title .total-price-tag').text(tPrice);
        //adding install prices
        var installPrice = 0;
        $('.cart-list-item .install-tag').each(function () {
            installPrice = parseInt(installPrice) + parseInt($(this).text());
        });
        $('.cart-side-secure--title .total-install-tag').text(installPrice);
        //sub total
        $('.cart-side-secure--title .sub-total-tag').text(tPrice + installPrice);

        $.ajax({
            url: $this.parent().attr('action'),
            type: "get",
            dataType: "json",
            data: {
                qty: value,
                productId: $this.attr('data-product_id'),
                price: value * productPrice,
                install_charges: value * installationCharges
            },
            success: function (response) {
                toastr.success("Quantity added.");
            },
            error: function (error) {
                toastr.error("There has been a error. Please try again.");
            }
        });
    });
    $('.remove-qty-form').on('submit', function (e) {
        var $this = $(this),
                productPrice = $this.parents('.cart-list-item').find('.price-tag').text(),
                installationCharges = $this.parents('.cart-list-item').find('.install-tag').text();

        var totalPrice = $('.cart-side-secure--title .total-price-tag').text(),
                tpsub = parseInt(totalPrice) - parseInt(productPrice);
        $('.cart-side-secure--title .total-price-tag').text(tpsub);
        var totalInstall = $('.cart-side-secure--title .total-install-tag').text(),
                tisub = parseInt(totalInstall) - parseInt(installationCharges);
        $('.cart-side-secure--title .total-install-tag').text(tisub);
        //sub total
        $('.cart-side-secure--title .sub-total-tag').text(tpsub + tisub);

        $.ajax({
            url: $this.attr('action'),
            type: "get",
            dataType: "json",
            data: {
                productId: $this.find('.product_id').val()
            },
            success: function (response) {
                $('.header-buttons--counter').text('');
                $('.header-buttons--counter').text(response.cartCount);
                if(!response.cartCount) {
                    $('.header-buttons--counter').text('0'); 
                    $('.checkOut').prop('disabled', true);  
                }
                $this.parents('.cart-list-item').remove();
                toastr.success("Product removed.");
            },
            error: function (error) {
                toastr.error("There has been a error. Please try again.");
            }
        });
        e.preventDefault();
    });
    $('.install-char-p input').on('click', function (e) {
        var $this = $(this);
        if ($this.parent().attr('data-check') == 1) {
//            $this.prop('checked',false);
            $this.parent().attr('data-check', 0);
            var installationCharges = $this.parents('.cart-list-item').find('.install-tag').text(),
                    totalPrice = $('.cart-side-secure--title .total-price-tag').text(),
                    totalInstall = $('.cart-side-secure--title .total-install-tag').text(),
                    tisub = parseInt(totalInstall) - parseInt(installationCharges);
            $('.cart-side-secure--title .total-install-tag').text(tisub);
            //sub total
            $('.cart-side-secure--title .sub-total-tag').text(parseInt(totalPrice) + tisub);

            $.ajax({
                url: $this.parent().attr('data-url'),
                type: "post",
                dataType: "json",
                data: {
                    productId: $this.parent().attr('data-id'),
                    cartType: 0//remove
                },
                success: function (response) {
                     $this.prop('checked',false);
                },
                error: function (error) {
                    toastr.error("There has been a error. Please try again.");
                }
            });
            e.preventDefault();
        }else{
//            $this.prop('checked',true);
            $this.parent().attr('data-check', 1);
            var installationCharges = $this.parents('.cart-list-item').find('.install-tag').text(),
                    totalPrice = $('.cart-side-secure--title .total-price-tag').text(),
                    totalInstall = $('.cart-side-secure--title .total-install-tag').text(),
                    tisub = parseInt(totalInstall) + parseInt(installationCharges);
            $('.cart-side-secure--title .total-install-tag').text(tisub);
            //sub total
            $('.cart-side-secure--title .sub-total-tag').text(parseInt(totalPrice) + tisub);

            $.ajax({
                url: $this.parent().attr('data-url'),
                type: "post",
                dataType: "json",
                data: {
                    productId: $this.parent().attr('data-id'),
                    cartType:1//add
                },
                success: function (response) {
                    $this.prop('checked',true);
                },
                error: function (error) {
                    toastr.error("There has been a error. Please try again.");
                }
            });
            e.preventDefault();
        }
    });
});
function addToCart(id) {
    $('.add_to_cart_selected-' + id).prop('disabled', 'disabled');
    var productPrice = $('.add_to_cart_selected-' + id).attr('data-product_price'),
            installationCharges = $('.add_to_cart_selected-' + id).attr('data-installation_charges');
    $.ajax({
        url: addToCartUrl,
        type: "GET",
        dataType: "json",
        data: {
            id: id,
            price: productPrice,
            install_charges: installationCharges
        },
        success: function (response) {
            $('.add_to_cart_selected-' + id).text('Added');
            $('.header-buttons--counter').text('');
            $('.header-buttons--counter').text(response.cartCount);
            toastr.success("Added to Cart Successfully.");
        },
        error: function (error) {
            $('.add_to_cart-' + id).prop('disabled', false);
            toastr.error("There has been a error. Please try again.");
        }
    });
}

