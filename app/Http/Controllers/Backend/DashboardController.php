<?php

namespace App\Http\Controllers\Backend;

use DB;
use Mail;
use Image;
use Response;
use App\Models\Car;
use App\Models\Order;
use App\Models\Product;
use App\Models\Company;
use App\Models\CarModel;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller {
    
    public function index() {
        
        return view('backend.dashboard');
    }

    public function allProduct() {

        $products = DB::select(
                        DB::raw("
                            SELECT products.id as productId, companies.name as companyName, cars.name as carName, car_models.model, products.name as productName, products.company as productCompany, products.price, products.genuine_part, products.installation_price, products.image as mainImage, products.is_activated
                            FROM companies
                            JOIN cars ON companies.id=cars.company_id
                            JOIN car_models On cars.id=car_models.car_id
                            JOIN products On car_models.id=products.car_model_id
                            WHERE products.deleted_at is Null
                            ORDER BY companyName
                        ")
        );
        
        return view('backend.allProduct')->with(['products' => $products]);
    }

    public function addProduct() {

        $carCompanies = Company::get();
        return view('backend.addProduct')->with(['carCompanies' => $carCompanies]);
    }

    public function getCars(Request $request) {

        $company = Company::where('name', $request->name)->first();
        if ($company) {
            $cars = Car::where('company_id', $company->id)->get();
            return Response::json(['cars' => $cars], 200);
        }
        return Response::json(['cars' => []], 200);
    }

    public function getModels(Request $request) {
        
        $car = Car::where('name', $request->name)->first();
        if ($car) {
            $carModels = CarModel::where('car_id', $car->id)->get();
            return Response::json(['carModels' => $carModels], 200);
        }
        return Response::json(['cars' => []], 200);
    }

    public function storeProduct(Request $request) {
        $this->validate($request, [
            'company' => 'required',
            'car' => 'required',
            'car_model' => 'required',
            'product_name' => 'required',
            'product_company' => 'required',
            'category' => 'required',
            'main_image' => 'mimes:jpeg,png,jpg',
            'product_images.*' => 'mimes:jpeg,png,jpg',
            'price' => 'required|numeric',
            'mrp' => 'required|numeric',
            'genuine_product' => 'required'
        ]);

        $company = Company::where('name', $request->company)->first();
        $car = Car::where('name', $request->car)->first();
        if ($car) {
            $model = CarModel::where('model', $request->car_model)->where('car_id', $car->id)->first();
        }

        DB::beginTransaction();
        if (!$company) {
            $company = new Company;
            $company->name = $request->company;
            if (!$company->save()) {
                DB::rollBack();
                return back()->withFlashDanger('Product not saved');
            }
        }

        if (!$car) {
            $car = new Car();
            $car->name = $request->car;
            $car->company_id = $company->id;
            if (!$car->save()) {
                DB::rollBack();
                return back()->withFlashDanger('Product not saved');
            }
        }

        if ((!isset($model)) || !$model) {
            $model = new CarModel();
            $model->model = $request->car_model;
            $model->car_id = $car->id;
            if (!$model->save()) {
                DB::rollBack();
                return back()->withFlashDanger('Product not saved');
            }
        }
        
        $product = new Product();
        $product->car_model_id = $model->id;
        $product->name = $request->product_name;
        $product->company = $request->product_company;
        $product->category = $request->category;
        $product->mrp = $request->mrp;
        $product->price = $request->price;
        $fileName = bin2hex(random_bytes(10)) . $request->main_image->getClientOriginalName();
        Image::make($request->main_image->getRealPath())->save(public_path('productImages') . '/' . $fileName);
        
//        $fileName = date('d-m-y-h-i-s-').preg_replace('/\s+/', '-',trim($request->main_image->getClientOriginalName()));
//                    $destinationPath = public_path('productImages') . '/' . $fileName;
//                    $request->main_image->move($destinationPath, $fileName);
                    
        $product->image = $fileName;
        $product->genuine_part = $request->genuine_product;
        $product->installation_price = $request->instalation_charges;
        if ($product->save()) {
            if (isset($request->product_images) && is_array($request->product_images)) {
                $productId = $product->id;
                foreach ($request->product_images as $image) {
                    $fileName = bin2hex(random_bytes(10)) . $image->getClientOriginalName();
                    Image::make($image->getRealPath())->save(public_path('productImages') . '/' . $fileName);
//                    $fileName = date('d-m-y-h-i-s-').preg_replace('/\s+/', '-',trim($image->getClientOriginalName()));
//                    $destinationPath = public_path('productImages') . '/' . $fileName;
//                    $image->move($destinationPath, $fileName);

                    $productImage = new ProductImage;
                    $productImage->product_id = $productId;
                    $productImage->image = $fileName;
                    if (!$productImage->save()) {
                        DB::rollBack();
                    }
                }
            }
            DB::commit();
            return back()->withFlashSuccess('Product added sucessfully');
        }
        DB::rollBack();
        return back()->withFlashDanger('Product not saved');
    }
    
    public function activateDeactivate($id) {
        
        $product = Product::where('id', $id)->first();
        if($product->is_activated) {
            Product::where('id', $id)->update(['is_activated' => 0]);
            return back()->withFlashWarning('Product deactivated sucessfully');
        } else {
            Product::where('id', $id)->update(['is_activated' => 1]);
            return back()->withFlashSuccess('Product activated sucessfully');
        }
    }
    

    public function deleteProduct($id) {
        $productId = '"productId":"'.$id.'"';
        if (!Order::where('product_detail', 'like', '%' . $productId . '%')->exists()) {
            Product::where('id', $id)->delete();
            return redirect()->route('admin.allProduct')->withFlashSuccess('Product deleted sucessfully');;
        }
        return redirect()->route('admin.allProduct')->withFlashDanger('Product not deleted as product has orders');
    }

    public function orders() {

        $allOrders = Order::with('user', 'address.city.state')->paginate(20);
        return view('backend.orders')->with(['allOrders' => $allOrders]);
    }
    
    public function orderDetail($id) {
        $data = Order::where('id',$id)->first();
        $detail = json_decode($data->product_detail);
        foreach($detail as $product) {
            $productId[] = $product->productId;
        }
        $products = Product::whereIn('id', $productId)->with('carModel.car.company')->get();
        return view('backend.order_details')->with(['products' => $products, 'orderId'=> $id]);
    }
    
    public function changeDeliveryStatus($id) {

        $order = Order::where('id', $id)->first();
        
        if ($order->delivery_status == 1) {
            if (Order::where('id', $id)->update(['delivery_status' => 0])) {

                return back()->withFlashDanger('Order status is pending');
            }
            return back()->withFlashDanger('Please try again later');
        } else {

            if (Order::where('id', $id)->update(['delivery_status' => 1])) {

                return back()->withFlashSuccess('Order delivered');
            }
            return back()->withFlashDanger('Please try again later');
        }
    }

    public function cancelOrder($id) {

        if (Order::where('id', $id)->update(['delivery_status' => 2])) {

            $data = Order::where('id', $id)->with('user')->first();

            $msg = 'Order cancelled';
            $body = 'Your order No : ' . $data->id . ' is cancalled by the vendor.';

            Mail::send('mail.sendMail', ['message2' => $msg,
                'name' => $data->user->first_name,
                'body' => $body], function($mg) use($msg, $data) {
                $mg->to($data->user->email)
                        ->subject($msg);
            });

            return back()->withFlashDanger('Order canceled');
        }
        return back()->withFlashDanger('Order not canceled');
    }

}
