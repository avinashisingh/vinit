<?php

namespace App\Http\Controllers\Frontend\User;

use Mail;
use Session;
use App\Models\City;
use App\Models\Order;
use App\Models\Address;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller {

    public function index() {
        return view('frontend.user.dashboard');
    }

    public function deliveryAddress() {
        
        if (Session::get('cartCount') != Null) {

            $addresses = Address::where('user_id', Auth::id())->with('city', 'state')->get();
            $cities = City::get();
            if ($addresses->count()) {
                return view('frontend.deliveryAddress')->with(['addresses' => $addresses, 'cities' => $cities]);
            }
            return view('frontend.deliveryAddress')->with(['cities' => $cities]);
        } return redirect()->route('frontend.index')->withFlashDanger('Cart is empty');
    }

    public function storeAddress(Request $request) {
        
        $this->validate($request, [
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'pin' => 'required|numeric',
        ]);
        
        $address = new Address;
        $address->user_id = Auth::id();
        $address->address = $request->address;
        $address->city_id = $request->city;
        $address->state_id = $request->state;
        $address->pincode = $request->pin;
        if ($address->save()) {
            $deliveryCharges = City::where('id', $request->city)->pluck('deliveryCharges');
            Session::put('addressId', $address->id);
            Session::put('deliveryCharges', $deliveryCharges[0]);
            return redirect()->route('frontend.user.reviewOrder')->withFlashSuccess('Address added sucessfully');
        }
        return back()->withFlashDanger('Address not saved');
    }

    public function reviewOrder() {
        if (Session::has('cart')) {
            $products = Session::get('cart');
            $totalPrice = 0;
            foreach ($products as $product) {
                $productIds[] = $product['productId'];
                $totalPrice = $totalPrice + $product['price'] + $product['install_charges'];
            }
            Session::put('billAmount', $totalPrice);
            Session::save();
            $cartProducts = Product::whereIn('id', $productIds)->with('carModel.car.company')->get();
            $address = Address::where('id', Session::get('addressId'))->with('city', 'state')->first();
            return view('frontend.place_order')->with(['products' => $cartProducts, 'address' => $address, 'totalPrice' => $totalPrice]);
        }
        return redirect()->route('frontend.index');
    }

    public function thankYou() {
        
        if (Session::has('cart')) {
            $order = Order::where('user_id', Auth::id())->where('address_id', Null)->where('bill_amount', Null)->first();
            if ($order) {
                if ($order->update(['product_detail' => json_encode(Session::get('cart')), 'address_id' => Session::get('addressId'), 'bill_amount' => Session::get('billAmount')])) {
                    
                   $msg = 'Order placed';
                        $body = 'A order is placed with order No : ' . $order->id . '.';

                        Mail::send('mail.sendMail', ['message2' => $msg,
                            'name' => 'Vinit',
                            'body' => $body], function($mg) use($msg) {
                            $mg->to('vinitgarg535@gmail.com')
                                    ->subject($msg);
                        });

                    Session::forget(['cart', 'bill_amount', 'addressId', 'cartCount']);
                    return view('frontend.thankyou');
                } else {
                    return back()->withFlashDanger('Danger');
                }
            } else {
                $order = new Order();
                $order->user_id = Auth::id();
                $order->product_detail = json_encode(Session::get('cart'));
                $order->address_id = Session::get('addressId');
                $order->bill_amount = Session::get('billAmount');
                $order->delivery_charges = Session::get('deliveryCharges');
                $order->payment_status = config('constant.inverse_payment.cod');
                $order->delivery_status = config('constant.inverse_delivery_status.pending');
                if ($order->save()) {
                    
                        $msg = 'Order placed';
                        $body = 'A order is placed with order No : ' . $order->id . '.';

                        Mail::send('mail.sendMail', ['message2' => $msg,
                            'name' => 'Vinit',
                            'body' => $body], function($mg) use($msg) {
                            $mg->to('vinitgarg535@gmail.com')
                                    ->subject($msg);
                        });
                    
                    Session::forget(['cart', 'bill_amount', 'addressId', 'cartCount']);
                    return view('frontend.thankyou');
                }
                return back()->withFlashDanger('Danger');
            }
        }
        return redirect()->route('frontend.index');
    }

    public function myOrders() {
        $orders = Order::where('user_id', Auth::id())->with('address.city.state')->paginate(20);
        return view('frontend.myOrders')->with(['orders' => $orders]);
    }
    public function myOrderDetails($id) {
        $data = Order::where('user_id', Auth::id())->where('id',$id)->first();
        $detail = json_decode($data->product_detail);
        foreach($detail as $product) {
            $productId[] = $product->productId;
        }
        $products = Product::whereIn('id', $productId)->with('carModel.car.company')->get();
        return view('frontend.myOrderDetails')->with(['products' => $products, 'orderId'=> $id]);
    }
}
