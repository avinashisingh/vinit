<?php

namespace App\Http\Controllers\Frontend;

use DB;
use Session;
use Response;
use App\Models\Car;
use App\Models\State;
use App\Models\Product;
use App\Models\Company;
use App\Models\CarModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class HomeController.
 */
class HomeController extends Controller {

    public function index() {

        $companies = Company::get();
        return view('frontend.index')->with(['companies' => $companies]);
    }

    public function getCars(Request $request) {
        $cars = Car::where('company_id', $request->id)->get();
        return Response::json(['cars' => $cars], 200);
    }

    public function getModels(Request $request) {
        $carModels = CarModel::where('car_id', $request->id)->get();
        return Response::json(['carModels' => $carModels], 200);
    }

    public function search(Request $request) {

        if ($request->has('model')) {

            $products = Product::where('car_model_id', $request->model)->where('is_activated', 1)->with('carModel.car.company')->get();
            return view('frontend.search')->with(['products' => $products]);
        } elseif ($request->has('category')) {

            $products = Product::where('category', $request->category)->where('is_activated', 1)->with('carModel.car.company')->get();
            return view('frontend.search')->with(['products' => $products]);
        }elseif ($request->has('company')) {

            $allProducts = DB::select(
                            DB::raw("
                            SELECT products.id as productId, companies.name as companyName, cars.name as carName, car_models.model, products.name as productName, products.company as productCompany, products.price, products.image, products.mrp
                            FROM companies
                            JOIN cars ON companies.id=cars.company_id
                            JOIN car_models On cars.id=car_models.car_id
                            JOIN products On car_models.id=products.car_model_id
                            WHERE companies.name =  '$request->company' AND products.is_activated = 1
                        ")
            );
            return view('frontend.search')->with(['allProducts' => $allProducts]);
        } else {

            $allProducts = DB::select(
                            DB::raw("
                            SELECT products.id as productId, companies.name as companyName, cars.name as carName, car_models.model, products.name as productName, products.company as productCompany, products.price, products.genuine_part, products.installation_price, products.image, products.mrp
                            FROM companies
                            JOIN cars ON companies.id=cars.company_id
                            JOIN car_models On cars.id=car_models.car_id
                            JOIN products On car_models.id=products.car_model_id
                            WHERE products.is_activated = 1
                            AND
                            companies.name LIKE '%" . $request->data . "%'
                            OR
                            cars.name LIKE '%" . $request->data . "%'
                            OR
                            products.name LIKE '%" . $request->data . "%'
                        ")
            );
            return view('frontend.search')->with(['allProducts' => $allProducts,
                        'searchWord' => $request->data]);
        }
    }

    public function showProduct($id = null) {

        if ($product = Product::where('id', $id)->where('is_activated', 1)->with('carModel.car.company', 'images')->first()) {
            return view('frontend.show_product')->with(['product' => $product]);
        }
        return back()->withFlashDanger('Your request cannot be completed right now. Please Try again later');
    }

    public function addToCart(Request $request) {
        if (Product::where('id', $request->id)->exists()) {


            if (Session::exists('cart')) {

                $cart_details = Session::get('cart');
                $key = array_search($request->id, array_column($cart_details, 'productId'));
                if ($key === false) {
                    $cart['productId'] = $request->id;
                    $cart['qty'] = 1;
                    $cart['price'] = $request->price;
                    $cart['install_charges'] = $request->install_charges;
                    Session::push('cart', $cart);
                } else {
                    return Response::json(['message' => 'Product already added'], 500);
                }
            } else {
                $cart[0]['productId'] = $request->id;
                $cart[0]['qty'] = 1;
                $cart[0]['price'] = $request->price;
                $cart[0]['install_charges'] = $request->install_charges;
                Session::put('cart', $cart);
            }
            $count = count(Session::get('cart'));
            Session::put('cartCount', $count);
            Session::save();
            return Response::json(['cartCount' => $count], 200);
        }
        return Response::json(500);
    }

    public function showCart() {

        if (Session::has('cart') && Session::has('cart') != NULL) {

            $products = Session::get('cart');
            foreach ($products as $product) {
                $productIds[] = $product['productId'];
            }
            $cartProducts = Product::whereIn('id', $productIds)->with('carModel.car.company')->get();
            return view('frontend.showCart')->with(['cartProducts' => $cartProducts]);
        }
        return back()->withFlashDanger('Cart is empty');
    }

    public function addQty(Request $request) {

        $products = Session::get('cart');
        foreach ($products as $key => $product) {
            if ($product['productId'] == $request->productId) {
                $products[$key]['qty'] = $request->qty;
                $products[$key]['price'] = $request->price;
                $products[$key]['install_charges'] = $request->install_charges;
                Session::put('cart', $products);
                return Response::json(200);
            }
        }
        return Response::json(500);
    }

    public function removeInstallCharges(Request $request) {

        $products = Session::get('cart');
        foreach ($products as $key => $product) {
            if ($product['productId'] == $request->productId) {
                if ($request->cartType == 0) {
                    $products[$key]['install_charges'] = 0;
                } else {
                    $eachProduct = Product::where('id', $request->productId)->first();
                    $products[$key]['install_charges'] = $eachProduct->installation_price;
                }
                Session::put('cart', $products);
                return Response::json(200);
            }
        }
        return Response::json(500);
    }

    public function removeProduct(Request $request) {

        $products = Session::get('cart');

        if (count($products) == 1) {
            Session::forget('cart');
            Session::forget('cartCount');

            return Response::json(['refresh' => true], 200);
        } else {
            foreach ($products as $key => $product) {
                if ($product['productId'] == $request->productId) {
                    unset($products[$key]);
                    $products = array_values($products);
                    Session::put('cart', $products);
                    $count = count($products);
                    Session::put('cartCount', $count);
                    return Response::json(['cartCount' => $count], 200);
                }
            }
        }
        return Response::json(500);
    }

    public function getState(Request $request) {

        $state = State::where('id', $request->id)->get();
        return Response::json(['state' => $state], 200);
    }

    public function aboutUs() {

        return view('frontend.aboutUs');
    }

    public function termsOfUse() {

        return view('frontend.termsOfUse');
    }

    public function privacyPolicy() {

        return view('frontend.privacyPolicy');
    }

    public function returnPolicy() {

        return view('frontend.returnPolicy');
    }

    public function getProfile() {

        return view('frontend.profile');
    }

}
