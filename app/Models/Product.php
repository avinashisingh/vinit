<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    
    public function carModel()
    {
        return $this->belongsTo('App\Models\CarModel');
    }
    
    public function orders() {
        return $this->hasMany('App\Models\Order');
    }
    
    public function images() {
        return $this->hasMany('App\Models\ProductImage');
    }
}
