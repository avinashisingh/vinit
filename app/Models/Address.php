<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;
    
    public function userDetail()
    {
        return $this->belongsTo('App\Models\Auth\User');
    }
    
    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }
    
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
}
