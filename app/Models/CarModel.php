<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use SoftDeletes;
    
    public function car()
    {
        return $this->belongsTo('App\Models\Car');
    }
    
    public function products() {
        return $this->hasMany('App\Models\Product');
    }
}
