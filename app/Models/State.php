<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;
    
    public function cities() {
        return $this->hasMany('App\Models\City');
    }
    
    public function stateAddress()
    {
        return $this->hasMany('App\Models\Address');
    }
}
