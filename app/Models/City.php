<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;
    
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }
    public function cityAddress()
    {
        return $this->hasMany('App\Models\Address');
    }
}
