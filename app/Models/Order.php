<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'delivery_status',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\Models\Auth\User');
    }
    
    public function address()
    {
        return $this->belongsTo('App\Models\Address');
    }
    
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
    
}
