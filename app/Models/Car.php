<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
    
     public function carModels() {
        return $this->hasMany('App\Models\CarModel');
    }
}
