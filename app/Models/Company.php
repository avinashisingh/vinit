<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model
{
 use SoftDeletes;
 
 public function cars() {
        return $this->hasMany('App\Models\Car');
    }
}
