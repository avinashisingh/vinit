<?php

return [
    'delivery_status' => [
        0 => 'Pending',
        1 => 'Delivered',
        2 => 'Cancelled'
    ],
    'inverse_delivery_status' => [
        'pending'   => 0,
        'delivered' => 1,
        'cancelled' => 2
    ],
    'payment' => [
      1 => 'COD',  
    ],
    'inverse_payment' => [
        'cod' => 1
    ]
];

