<?php

use App\Models\State;

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::create([
            'name' => 'Punjab',
        ]);
        State::create([
            'name' => 'Chandigarh',
        ]);
        State::create([
            'name' => 'Haryana',
        ]);
    }
}
