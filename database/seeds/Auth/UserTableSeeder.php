<?php

use App\Models\Auth\User;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'first_name'        => 'Vinit',
            'last_name'         => 'Garg',
            'email'             => 'Vinitgarg535@gmail.com',
            'password'          => 'Vinit@45chd',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'phone_number'      => 8727852806,
        ]);

        User::create([
            'first_name'        => 'Backend',
            'last_name'         => 'User',
            'email'             => 'executive@executive.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'phone_number'          => 9876543211,
        ]);

        User::create([
            'first_name'        => 'Default',
            'last_name'         => 'User',
            'email'             => 'user@user.com',
            'password'          => 'secret',
            'confirmation_code' => md5(uniqid(mt_rand(), true)),
            'confirmed'         => true,
            'phone_number'          => 9876543212,
        ]);

        $this->enableForeignKeys();
    }
}
