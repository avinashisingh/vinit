<?php
use App\Models\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        City::create([
            'state_id' => 1,
            'name' => 'Mohali',
            'deliveryCharges' => '100',
        ]);
        City::create([
            'state_id' => 2,
            'name' => 'Chandigarh',
            'deliveryCharges' => '100',
        ]);
        City::create([
            'state_id' => 1,
            'name' => 'Kharar',
            'deliveryCharges' => '100',
        ]);
        City::create([
            'state_id' => 3,
            'name' => 'Panchkula',
            'deliveryCharges' => '100',
        ]);
        City::create([
            'state_id' => 1,
            'name' => 'Manimajra',
            'deliveryCharges' => '100',
        ]);
        City::create([
            'state_id' => 1,
            'name' => 'zirakpur',
            'deliveryCharges' => '100',
        ]);
    }

}
