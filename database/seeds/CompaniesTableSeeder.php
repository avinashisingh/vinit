<?php

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompaniesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Company::create([
            'name' => 'Nissan',
        ]);
        Company::create([
            'name' => 'Renault',
        ]);
        Company::create([
            'name' => 'BMW',
        ]);
        Company::create([
            'name' => 'Skoda',
        ]);
        Company::create([
            'name' => 'Ford',
        ]);
        Company::create([
            'name' => 'Chevrolet',
        ]);
        Company::create([
            'name' => 'Volkswagen',
        ]);
        Company::create([
            'name' => 'Mahindra',
        ]);
        Company::create([
            'name' => 'Maruti Suzuki',
        ]);
        Company::create([
            'name' => 'Hyundai',
        ]);
        Company::create([
            'name' => 'Toyota',
        ]);
        Company::create([
            'name' => 'Honda',
        ]);
        Company::create([
            'name' => 'Tata',
        ]);
    }

}
