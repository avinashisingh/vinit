<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\ContactController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\Frontend\User\DashboardController;

/*
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('get-cars', [HomeController::class, 'getCars'])->name('getCars');
Route::get('get-models', [HomeController::class, 'getModels'])->name('getModels');
Route::get('search', [HomeController::class, 'search'])->name('search');
Route::get('addToCart', [HomeController::class, 'addToCart'])->name('addToCart');
Route::get('show-cart', [HomeController::class, 'showCart'])->name('showCart');
Route::get('get-state', [HomeController::class, 'getState'])->name('getState');
Route::get('enter-email', [HomeController::class, 'enterEmail'])->name('enterEmail');
Route::get('about-us', [HomeController::class, 'aboutUs'])->name('aboutUs');
Route::get('terms-of-use', [HomeController::class, 'termsOfUse'])->name('termsOfUse');
Route::get('privacy-policy', [HomeController::class, 'privacyPolicy'])->name('privacyPolicy');
Route::get('return-policy', [HomeController::class, 'returnPolicy'])->name('returnPolicy');
Route::get('contact', [ContactController::class, 'index'])->name('contact');
Route::post('contact/send', [ContactController::class, 'send'])->name('contact.send');
Route::get('profile', [HomeController::class, 'getProfile'])->name('getProfile');
Route::get('product/{id?}', [HomeController::class, 'showProduct'])->name('showProduct');
Route::get('add-qty', [HomeController::class, 'addQty'])->name('addQty');
Route::get('remove-product', [HomeController::class, 'removeProduct'])->name('removeProduct');
Route::post('remove-install-charges', [HomeController::class, 'removeInstallCharges'])->name('removeInstallCharges');

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 * These routes can not be hit if the password is expired
 */
Route::group(['middleware' => ['auth', 'password_expires']], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
//        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
        Route::get('my-orders', [DashboardController::class, 'myOrders'])->name('myOrders');
        Route::get('my-order-details/{id}', [DashboardController::class, 'myOrderDetails'])->name('myOrderDetails');
        Route::get('delivery-address', [DashboardController::class, 'deliveryAddress'])->name('deliveryAddress');
        Route::post('store-address', [DashboardController::class, 'storeAddress'])->name('storeAddress');
        Route::get('review-order', [DashboardController::class, 'reviewOrder'])->name('reviewOrder');
        Route::get('thank-you', [DashboardController::class, 'thankYou'])->name('thankYou');

        /*
         * User Account Specific
         */
        Route::get('account', [AccountController::class, 'index'])->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
    });
});
