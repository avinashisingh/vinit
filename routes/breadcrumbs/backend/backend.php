<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.allProduct', function ($trail) {
    $trail->push(__('strings.backend.allProduct.title'), route('admin.allProduct'));
});

Breadcrumbs::for('admin.addProduct', function ($trail) {
    $trail->push(__('strings.backend.addProduct.title'), route('admin.addProduct'));
});

Breadcrumbs::for('admin.orders', function ($trail) {
    $trail->push(__('strings.backend.orders.title'), route('admin.orders'));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
