<?php

use App\Http\Controllers\Backend\DashboardController;

/*
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('all-product', [DashboardController::class, 'allProduct'])->name('allProduct');
Route::get('add-product', [DashboardController::class, 'addProduct'])->name('addProduct');
Route::get('get-cars', [DashboardController::class, 'getCars'])->name('getCars');
Route::get('get-models', [DashboardController::class, 'getModels'])->name('getModels');
Route::post('store-product', [DashboardController::class, 'storeProduct'])->name('storeProduct');
Route::get('delete-product/{id}', [DashboardController::class, 'deleteProduct'])->name('deleteProduct');
Route::get('orders', [DashboardController::class, 'orders'])->name('orders');
Route::get('order-detail/{id}', [DashboardController::class, 'orderDetail'])->name('orderDetail');
Route::get('change-delivery-status/{id}', [DashboardController::class, 'changeDeliveryStatus'])->name('changeDeliveryStatus');
Route::get('activate-deactivate/{id}', [DashboardController::class, 'activateDeactivate'])->name('activateDeactivate');
Route::get('cancel-order/{id}', [DashboardController::class, 'cancelOrder'])->name('cancelOrder');
