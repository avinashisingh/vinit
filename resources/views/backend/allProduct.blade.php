@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('content')
<style>
    #main-table img {
        width: 100px;
    }
    #main-table {
        text-align: center;
    }
    #main-table a {
        color: red;
        font-size: 18px;
        margin-left: 7px;
    }
    #main-table td {
        vertical-align: middle !important;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    All Products 
                </h4>
            </div><!--col-->

            <div class="col-sm-7" style="text-align: right;">
                <a href="{{route('admin.addProduct')}}" class="button btn btn-success ">Add Product</a>
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4" id="main-table">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Company</th>
                                <th>Car</th>
                                <th>Car Model</th>
                                <th>Product</th>
                                <th>Product Company</th>
                                <th>Image</th>
                                <th>price</th>
                                <th>Genuine Part</th>
                                <th>Installation Price</th>
                                @if(Auth::user()->isAdmin())
                                <th>Actions</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key=>$product)
                            <tr>
                                <td>{{ $key +1 }}</td>
                                <td>{{ $product->companyName }}</td>
                                <td>{{ $product->carName }}</td>
                                <td>{{ $product->model }}</td>
                                <td>{{ $product->productName }}</td>
                                <td>{{ $product->productCompany }}</td>
                                <td>
                                    <img src="{{ asset('productImages/'.$product->mainImage) }}" class="img-responsive">
                                </td>
                                <td>{{ $product->price }}</td>
                                @if($product->genuine_part == 1)
                                <td>Yes</td>
                                @else
                                <td>No</td>
                                @endif
                                <td>{{ $product->installation_price }}</td>
                                @if(Auth::user()->isAdmin())
                                <td>
                                    <a href="{{route('admin.activateDeactivate', $product->productId)}}" title="Activate/Deactivate" class="btn btn-sm">
                                        <?php if ($product->is_activated) { ?>
                                            <i class="fa fa-toggle-on text-success"></i>
                                        <?php } else { ?>
                                            <i class="fa fa-toggle-off text-warning"></i>
                                        <?php } ?>
                                    </a>
                                    <a href="{{route('admin.deleteProduct', $product->productId)}}"  title="Delete">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                </td>
                                @endif
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
