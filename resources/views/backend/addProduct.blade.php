@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
<style>
    .product_labels {
        text-align: right;
        vertical-align: middle;
        line-height: 2;
    }
    .input-main{
        margin-bottom: 15px;
    }
    .box-info{
        margin-bottom: 15px;
    }
    #company:focus {
        outline: #8ad4ee;
    }
    .bg-info {
        min-width: 177px;
    }
    .category-input-g .input-group{
        padding: 0;
    }
    .category-input-g .input-group-append{
        width: 100%;
    }
    .category-input-g #category{
        padding: 6px 12px;
        width: 100%;
    }
</style>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Create Product</h3>
    </div><!-- /.box-header -->
    <form action="{{ route('admin.storeProduct') }}" method="POST" enctype="multipart/form-data">
        <input type='hidden' name='_token' value='{{ csrf_token() }}'>
        <div class="box-body">
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Company
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="text" required class="form-control" id="company-input" name="company" placeholder="Please Enter Company Name...." aria-label="company">
                        <div class="input-group-append">
                            <select id="company" class="bg-info" >
                                <option value="">Select Company</option>
                                <?php
                                if (isset($carCompanies) && count($carCompanies) > 0) {
                                    foreach ($carCompanies as $company) {
                                        ?>
                                        <option value="{{$company->name}}">{{$company->name}}</option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Car
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="text" required class="form-control" id="car-input" name="car" placeholder="Please Enter Car...." aria-label="company">
                        <div class="input-group-append">
                            <select id="car" class="bg-info">
                                <option value="">Select Car</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Car Model
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="text" required class="form-control" id="car-model-input" name="car_model" placeholder="Please Enter Car Model...." aria-label="company">
                        <div class="input-group-append">
                            <select id="car-model" class="bg-info">
                                <option value="">Select Car Model</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Product Name
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="text" required class="form-control" id="product-name-input" name="product_name" placeholder="Please Enter Product Name...." aria-label="company">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Product Company
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="text" required class="form-control" id="product-company-input" name="product_company" placeholder="Please Enter Product Company...." aria-label="company">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    MRP
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="number" required class="form-control" name="mrp" placeholder="Please Enter Price...." aria-label="company">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Discounted Price
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="number" required class="form-control" name="price" placeholder="Please Enter Discounted Price...." aria-label="company">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Product Main Image
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="file" required class="form-control" name="main_image" placeholder="Please Choose Cover Image....">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Product Images
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="file" required class="form-control" multiple name="product_images[]" placeholder="Please Choose Product Images....">
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Category
                </div>
                <div class="col-sm-7 pull-left category-input-g">
                    <div class="input-group form-control">
                        <div class="input-group-append">
                            <select id="category" class="bg-info" name="category" >
                                <option value="1">Maintenance / Service Parts</option>
                                <option value="2">Brakes</option>
                                <option value="3">Suspension</option>
                                <option value="4">Body</option>
                                <option value="5">Accessories</option>
                                <option value="6">Electric Components & Lights</option>
                                <option value="7">Engine</option>
                                <option value="8">Trims</option>
                                <option value="9">Interior</option>
                                <option value="10">Steering</option>
                                <option value="11">Transmission</option>
                                <option value="12">Fuel Supply System</option>
                                <option value="13">Air Conditioning / Heater</option>
                                <option value="14">Wheel Drive</option>
                                <option value="15">Exhaust System</option>
                                <option value="16">Universal</option>
                                <option value="17">Lubricants</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Genuine Product
                </div>
                <div class="col-sm-7 pull-left">
                    <input type="radio"  name="genuine_product" value="1"checked> Yes
                    <input type="radio"  name="genuine_product" value="0"> No
                </div>
            </div>
            <div class="row input-main">
                <div class="col-sm-3 pull-left product_labels">
                    Instalation Charges
                </div>
                <div class="col-sm-7 pull-left">
                    <div class="input-group">
                        <input type="number" required class="form-control" name="instalation_charges" placeholder="Please Enter Instalation Charges....">
                    </div>
                </div>
            </div>
        </div>
        <div class="row input-main">
            <div class="col-sm-3 ">
            </div>
            <div class="col-sm-7 pull-left">
                <div class="box box-info">
                    <!-- /.box-body -->
                    <div class="box-body">
                        <div class="float-right">
                            <button class="btn button btn-success" style="background-color: #17a2b8 !important;" type="submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        {{ html()->form()->close() }}
</div>
@endsection