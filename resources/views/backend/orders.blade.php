@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('content')
<?php // dd($allOrders); ?>
<style>
    #main-table img {
        width: 100px;
    }
    #main-table {
        text-align: center;
    }
    #main-table td {
        vertical-align: middle !important;
    }
    #action-col {
        width: 75px !important;
    }
</style>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    All Orders
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4" id="main-table">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Order No</th>
                                <th>User Name</th>
                                <th>Bill Amount</th>
                                <th>Delivery Chargers</th>
                                <th>Total Payment</th>
                                <th>Address</th>
                                <th>Payment Status</th>
                                <th>Status</th>
                                <th id="action-col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($allOrders as $key=>$order)
                            <tr class="each-order">
                                <td>{{ $key +1 }}</td>
                                <td>{{ $order->id }}</td>
                                <td>{{ $order->user->first_name. ' '. $order->user->last_name }}</td>
                                <td>{{ $order->bill_amount }}</td>
                                <td>{{ $order->delivery_charges }}</td>
                                <td>{{ $order->bill_amount + $order->delivery_charges }}</td>
                                <td>{{ $order->address->address. ' '. $order->address->city->name. ' '. $order->address->city->state->name }}</td>
                                <td>{{Config::get('constant.payment.1')}}</td>
                                @if($order->delivery_status == Config::get('constant.inverse_delivery_status.pending'))
                                <td class="delivery-status">{{Config::get('constant.delivery_status.0')}}</td>
                                @elseif($order->delivery_status == Config::get('constant.inverse_delivery_status.delivered')) 
                                <td class="delivery-status">{{Config::get('constant.delivery_status.1')}}</td>
                                @else
                                <td class="delivery-status">{{Config::get('constant.delivery_status.2')}}</td>
                                @endif
                                <td> 
                                    <a href="{{route('admin.orderDetail', $order->id)}}" title="Order Details" class="">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    @if(Auth::user()->isAdmin())
                                    @if($order->delivery_status != 2)
                                    <a href="{{route('admin.changeDeliveryStatus', $order->id)}}" title="Change Delivery Status" class="changeDeliveryStatus">
                                        <i class="fa fa-shopping-cart"></i>
                                    </a>
                                    @endif
                                    @if($order->delivery_status == 0)
                                    <a href="{{route('admin.cancelOrder', $order->id)}}" title="Cancel Delivery" class="cancelDelivery">
                                        <i class="fa fa-times" aria-hidden="true"></i>
                                    </a>
                                    @endif
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
