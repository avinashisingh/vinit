@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))

@section('content')
<?php // dd($allOrders); ?>
<style>
    #main-table img {
        width: 100px;
    }
    #main-table {
        text-align: center;
    }
    #main-table a {
        color: red;
    }
    #main-table td {
        vertical-align: middle !important;
    }
</style>
<?php
//       dd($products);
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-5">
                <h4 class="card-title mb-0">
                    Order details for order: #{{$orderId}}
                </h4>
            </div><!--col-->

            <div class="col-sm-7">
            </div><!--col-->
        </div><!--row-->

        <div class="row mt-4" id="main-table">
            <div class="col">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Product Company</th>
                                <th>Car Company</th>
                                <th>Car</th>
                                <th>Car Modal</th>
                                <th>Price</th>
                                <th>Installation Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $key=>$product)
                            <tr>
                                <td>{{ $key +1 }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->company }}</td>
                                <td>{{ $product->carModel->car->company->name }}</td>
                                <td>{{ $product->carModel->car->name }}</td>
                                <td>{{ $product->carModel->model }}</td>
                                <td>{{ $product->price }}</td>
                                <td>{{ $product->installation_price }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!--col-->
        </div><!--row-->
        <div class="row">
            <div class="col-7">
                <div class="float-left">
                </div>
            </div><!--col-->

            <div class="col-5">
                <div class="float-right">
                </div>
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
</div><!--card-->
@endsection
