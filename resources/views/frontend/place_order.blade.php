@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php // dd($address->city->deliveryCharges); ?>
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">

<div id="c_step_4" class="check-panel pristine active">
    <div class="check-panel-title btn-change-event">
        <div class="">
            <div class="table-title">
                <span class="title-content">Place Order</span>
            </div>
            <div class=""></div>
            <div>
                <button class="btn btn-change btn-change-event">
                    <span class="mobile_hide">change</span>
                    <i class="desktop_hide icon-retweet"></i>
                </button>            </div>
        </div>
    </div>
    <div class="check-panel-body">
        <div class="cart-side-secure--title text-left mv-15 place-priceses">
            <p>Total :<b> <span class="icon-inr"></span> <span class="total-price-tag">{{ $totalPrice }}</span></b></p>
            <p>Delivery Charges :<b> <span class="icon-inr"></span> <span class="total-install-tag">{{ $address->city->deliveryCharges }}</span></b></p>
            <p>Subtotal :<b> <span class="icon-inr"></span> <span class="sub-total-tag">{{ $total_delivery_p = $totalPrice + $address->city->deliveryCharges }}</span></b></p>
        </div>

        <form action="{{route('frontend.user.thankYou')}}" method="get" class="form-payment" id="checkout-form-payment"> 
            <div class="clear_fix">
                <div class="payment-method inr"> 
                    <div><b>Payment method for INR</b></div>
                    <div class="razorpay-method-method-item">
                        <input type="radio" checked="checked" id="forms-input-razorpay">
                        <label class="" for="forms-input-razorpay">
                            <div><span></span></div>
                            <div><i></i></div>
                            <div>
                                Cash on Delivery
                            </div>
                            <div class="checkout-price-block  ">
                                <div class="">INR <?php echo $total_delivery_p; ?></div>
                            </div>
                        </label>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn big-btn button-blue btn-block-sm" value="place order">
        </form>
    </div>
</div>

@endsection

