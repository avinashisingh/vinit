@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link href="{{ asset('css/frontend/aboutUs.css') }}" rel="stylesheet">

<div>
    <div class="messages">
    </div>        
    <div class="icons_block">
        <h1><strong>About Us</strong></h1>

<!--        <p style="text-align: center;"><b>
                <img alt="" src="" style="max-width: 100%; height: autopx;">
            </b></p>-->



        <p style="text-align: justify;">Vinit Automobile is chandigarh’s largest online marketplace for auto components buyers. Founded in the year 2015, the company has a huge database of genuine and aftermarket replacement parts. With our aim to be&nbsp;“Your Car Parts Expert”, we strive to become a pioneer in the automotive parts industry. Being an unorganized sector in the country, sometimes it becomes very difficult for the car owners to find replacement parts. Our platform offers a trusted channel for car owners in order to reap the maximum benefits out of it. Customers can find the required automotive components using our online catalogue and can make their search through VIN, brand or part number.</p>

        <p style="text-align: justify;">Since 100% customer satisfaction is our utmost priority, we only deal in high-quality genuine or aftermarket parts of over 3000 national or international brands. Our expert team is dedicated to redefining&nbsp;the Indian spare parts industry. Born out of the simple idea customers can find the exact parts with transparent pricing and on time delivery, surely a win-win situation for all concerned.</p>


        <h3><strong>What We offer</strong></h3>

        <p><span style="font-size:14px;">&nbsp; a. We offer over 3.2 crores unique spare parts in the catalogue, including both Genuine and Aftermarket.</span></p>

        <p><span style="font-size:14px;">&nbsp; b. We have over 2.7 crores spare parts available for purchase with prices</span></p>

        <p><span style="font-size:14px;">&nbsp; c. We have 4.8 crores spare parts offers listed from more than 400 suppliers and manufacturers throughout the globe.</span></p>

        <p><span style="font-size:14px;">&nbsp; d. We deliver products to Indian customers from Europe, USA, Japan, China, UAE and South Korea.&nbsp;</span></p>

        <p><span style="font-size:14px;">&nbsp; e. We have over 3000 spare parts brands to choose from.</span></p>

        <p><span style="font-size:14px;">&nbsp; f. Customer-Friendly Return Policy</span></p>

        <p><span style="font-size:14px;">&nbsp; g. 100% guarantee of the products to be applied to your car</span></p>

        <p>&nbsp;</p>

        <h3><strong>Our Vision</strong></h3>

        <p style="text-align: justify;"><span style="font-size:14px;">We are guided by the Vision to make Automobile Service Industry Organised where people can find the best-suited automobile parts just with the few mouse clicks.</span></p>

        <h3>&nbsp;</h3>

        <h3><strong>Our Aim</strong></h3>


        <p style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;">We are driven by the aim to organise market of spare parts in India and become the #1 online destination for everybody interested in the auto service industry. We are in a process to make the spare parts market more dynamic and transparent for everybody.&nbsp;</span></span></p>

        <p>&nbsp;</p>

        <blockquote>
            <h3><b>We are an “</b><span style="color:#3366ff;"><b>Indian Company Operating for Indian Customers Only</b></span><b>”</b></h3>
        </blockquote>

        <p>&nbsp;</p>

        <h3 style="text-align: center;"><strong><span style="color:#0000FF;">We pursue our aims only in line with our values</span></strong></h3>

        <p>&nbsp;</p>


        <ul class="clear_fix" data-name="aboutUs">
            <li>
                <div class="image" style="text-align: justify;"><img alt="Accountability" src="https://boodmo.com/media/images/articles/e4a672b.png"></div>

                <div>
                    <p class="name" style="text-align: justify;">Accountability</p>

                    <p class="description" style="text-align: justify;"><span style="font-size:14px;">Everyone’s responsibility is a condition of a sustainable environment</span></p>
                </div>
            </li>
            <li>
                <div class="image" style="text-align: justify;"><img alt="High quality" src="https://boodmo.com/media/images/articles/251ed4f.png"></div>

                <div>
                    <p class="name" style="text-align: justify;">High quality</p>

                    <p class="description" style="text-align: justify;"><span style="font-size:14px;">Only continuous attention to the smallest details makes the great product</span></p>
                </div>
            </li>
            <li>
                <div class="image" style="text-align: justify;"><img alt="Сollaboration" src="https://boodmo.com/media/images/articles/f20e376.png"></div>

                <div>
                    <p class="name" style="text-align: justify;">Сollaboration</p>

                    <p class="description" style="text-align: justify;"><span style="font-size:14px;">Our suppliers, clients and employees are partners which rely on each other</span></p>
                </div>
            </li>
            <li>
                <div class="image" style="text-align: justify;"><img alt="Innovation" src="https://boodmo.com/media/images/articles/b982eb5.png"></div>

                <div>
                    <p class="name" style="text-align: justify;">Innovation</p>

                    <p class="description" style="text-align: justify;"><span style="font-size:14px;">We strive&nbsp;to introduce fresh ideas to bring the best experience for users</span></p>

                    <p class="description" style="text-align: justify;">&nbsp;</p>
                </div>
            </li>
        </ul>
    </div>
</div>


@endsection