@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">
<div id="content-cart">
    <div class="my-page">
        <div class="clear_fix">
            <h2>Shopping Cart</h2>
        </div>
        <div class="">
            <div class="cart-content col-sm-8">
                <div class="cart-list">
                    <?php
                    $totalPrice = 0;
                    $installPrice = 0;
                    $cart_details = session('cart');
                    ?>
                    @foreach($cartProducts as $product)
                    <?php
                    $key = array_search($product->id, array_column($cart_details, 'productId'));
                    if (isset($key) && $key !== false) {
                        $totalPrice = $totalPrice + $product->price;
                        $installPrice = $installPrice + $cart_details[$key]['install_charges'];
                        ?>
                        <div class="cart-list-item clearfix">
                            <div class="col-sm-6 pull-left product_image">
                                <a href="{{ route('frontend.showProduct', $product->id) }}">
                                    <img src= "{{asset('productImages/'.$product->image) }}" alt="{{ $product->name}}">
                                </a>
                            </div>
                            <div class="col-sm-6 pull-left product_description">
                                <h1 class="cart-description--title mb-15">
                                    <a href="{{ route('frontend.showProduct', $product->id) }}">{{ $product->name}}</a>
                                </h1>
                                <div class="cart-list-item-price">
                                    <div class="old_price">
                                        <p><b>Price:</b> <span class="icon-inr"></span> <span class="price-tag">{{ $product->price }}</span> </p>
                                        <p><b>Installation<small>(per piece)</small>:</b> 
                                            <span class="icon-inr"></span> <span class="install-tag">{{ $product->installation_price }}</span></p>
                                    </div>
                                </div>
                                <div>
                                    <p> Brand: {{ $product->carModel->car->company->name}}</p>
                                    <p> Car: {{ $product->carModel->car->name}}</p>
                                    @if($product['genuine_part'] == 1)
                                    <p> Genuine Part: Yes</p>
                                    @endif
                                    <div class="mt-15">
                                        <label class="text-top"><span>Qty:</span>
                                            <form class="change-qty-form" method="get" action="{{route('frontend.addQty')}}" novalidate="novalidate">
                                                <select name="qty" class="" data-product_id="{{$product->id}}" data-product_price="{{$product->price}}" data-installation_charges="{{$product->installation_price}}">
                                                    <option value="1" <?php echo ($cart_details[$key]['qty'] == 1 ? 'selected' : '') ?>>1</option>
                                                    <option value="2" <?php echo ($cart_details[$key]['qty'] == 2 ? 'selected' : '') ?>>2</option>
                                                    <option value="3" <?php echo ($cart_details[$key]['qty'] == 3 ? 'selected' : '') ?>>3</option>
                                                    <option value="4" <?php echo ($cart_details[$key]['qty'] == 4 ? 'selected' : '') ?>>4</option>
                                                    <option value="5" <?php echo ($cart_details[$key]['qty'] == 5 ? 'selected' : '') ?>>5</option>
                                                    <option value="6" <?php echo ($cart_details[$key]['qty'] == 6 ? 'selected' : '') ?>>6</option>
                                                    <option value="7" <?php echo ($cart_details[$key]['qty'] == 7 ? 'selected' : '') ?>>7</option>
                                                    <option value="8" <?php echo ($cart_details[$key]['qty'] == 8 ? 'selected' : '') ?>>8</option>
                                                    <option value="9" <?php echo ($cart_details[$key]['qty'] == 9 ? 'selected' : '') ?>>9</option>
                                                    <option value="10" <?php echo ($cart_details[$key]['qty'] == 10 ? 'selected' : '') ?>>10</option>
                                                </select>
                                            </form>
                                        </label>
                                    </div>
                                    <div class="mt-15">
                                        <p class="install-char-p" data-url="{{route('frontend.removeInstallCharges')}}" data-id="{{$product->id}}" data-check="<?php echo ($cart_details[$key]['install_charges'] == 0 ? '0' : '1') ?>">
                                            <input type='checkbox' class='install_char-check' <?php echo ($cart_details[$key]['install_charges'] == 0 ? '' : 'checked') ?>> Installation Charges(Fitting)</p>                                     
                                    </div>
                                </div>
                                <form method="get" action="{{route('frontend.removeProduct')}}" class="remove-qty-form">
                                    <input type="hidden" name="product_id" class="product_id" value="{{$product->id}}"/>
                                    <button class="cart-list-item--remove" type="submit">Remove Item</button>
                                </form>
                            </div>
                        </div>
                    <?php } ?>
                    @endforeach

                </div>
            </div>
            <div id="cart-sidebar" class="cart-sidebar single-product col-sm-4">
                <div class="cart-side-panel text-center" style="height: 100%;">
                    <div class="cart-side-secure">
                        <img src="{{asset('img/frontend/secure_label.png') }}" alt="100% Secure Online Checkout">
                        <img src="{{asset('img/frontend/10_days_returns.png') }}" alt="10 DAYS ASSURED RETURN" class="pb-20">
                    </div>
                    <div class="cart-side-secure--title text-left mv-15">
                        <p>Total :<b> <span class="icon-inr"></span> <span class="total-price-tag">{{ $totalPrice }}</span></b></p>
                        <p>Installation Charges(Fitting) :<b> <span class="icon-inr"></span> <span class="total-install-tag">{{ $installPrice }}</span></b></p>
                        <p>Subtotal :<b> <span class="icon-inr"></span> <span class="sub-total-tag">{{ $totalPrice + $installPrice }}</span></b></p>
                    </div>
                    @if(Auth::id())
                    <button onclick="window.location.href='{{route('frontend.user.deliveryAddress')}}'" class="btn button big-blue-btn btn-block checkOut" type="button">proceed to checkout</button>
                    @else 
                    <button onclick="window.location.href='{{route('frontend.auth.login')}}'" class="btn button big-blue-btn btn-block checkOut" type="button">proceed to checkout</button>
                    @endif
                    <a href="{{route('frontend.index')}}" class="btn mt-15">Continue Shopping</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')

@endpush