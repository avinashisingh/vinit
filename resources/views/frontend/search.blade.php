@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php
// dd(session('cart'));
$cart_details = session('cart');
?>
<link href="{{ asset('css/frontend/product_listing.css') }}" rel="stylesheet">
@if(isset($searchWord))
<div class="content_block">
    <div class="title">
        <h1>Search results for "{{$searchWord}}"</h1>
    </div>
</div>
@endif
<div class="searched_products_main">
    <div class="table_content">
        <div class="content_block right_side">
            <div class="">
                <ul class="list_view_block ">
                    @if(isset($allProducts))
                    @if($allProducts != null)
                    @foreach($allProducts as $product)
                    <li class="clear_fix">
                        <div class="fl_left table">
                            <div class="image">
                                <a href="{{ route('frontend.showProduct', $product->productId) }}" class="relative" >
                                    <img src="{{asset('productImages/'.$product->image) }}" alt="{{$product->productName}}">
                                </a>
                            </div>
                            <div class="information">
                                <div class="inline_block search_result_information">
                                    <div class="name">
                                        <a href="{{ route('frontend.showProduct', $product->productId) }}" >{{$product->productName}}</a>
                                    </div>
                                    <div class="gray">
                                        <div>
                                            <b>Company:</b> {{$product->companyName}}
                                        </div>
                                        <div>
                                            <b>Car:</b> {{$product->carName}}
                                        </div>
                                        <div>
                                            <b>Model:</b> {{$product->model}}
                                        </div>
                                        <div>
                                            <b>Product Brand:</b> {{$product->productCompany}}
                                        </div>
                                        @if($product->mrp == $product->price)
                                        <div>
                                            <b>MRP:</b> {{$product->price}}
                                        </div>
                                        @else
                                        <div>
                                            <b>MRP:</b> {{$product->mrp}}
                                        </div>
                                        <div>
                                            <b>Discounted Price:</b> {{$product->price}}
                                        </div>
                                        @endif
                                        <div>
                                            <b>Genuine Product:</b> @if($product->genuine_part == 1) Yes @else No @endif
                                        </div>
                                        <div>
                                            <b>Installation Charges(Fitting):</b> {{$product->installation_price}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (!empty($cart_details)) {
                            $key = array_search($product->productId, array_column($cart_details, 'productId'));
                        }
                        if (isset($key) && $key !== false) {
                            ?>
                        <button style="background-color: #16c7f6 !important;" class="buy fl_right add_to_cart add_to_cart_selected" disabled="disabled">
                                Added
                            </button>
                        <?php } else { ?>
                            <button style="background-color: #16c7f6 !important;" class="buy fl_right add_to_cart add_to_cart_selected-{{$product->productId}}" data-product_price="{{$product->price}}" data-installation_charges="{{$product->installation_price}}" onclick="addToCart({{$product->productId}})">
                                Add to Cart
                            </button>
                        <?php } ?>
                    </li>
                    @endforeach
                    @else
                    <li class="clear_fix">
                        <div class="fl_left table">
                            No Data
                        </div>
                    </li>
                    @endif
                    @else
                    @if($products->isNotEmpty())
                    @foreach($products as $product)
                    <li class="clear_fix">
                        <div class="fl_left table">
                            <div class="image">
                                <a href="{{ route('frontend.showProduct', $product->id) }}" class="relative" >
                                    <img src="{{asset('productImages/'.$product->image)}}" alt="{{$product->productName}}">
                                </a>
                            </div>
                            <div class="information">
                                <div class="inline_block search_result_information">
                                    <div class="name">
                                        <a href="{{ route('frontend.showProduct', $product->id) }}" >{{$product->name}}</a>
                                    </div>
                                    <div class="gray">
                                        <div>
                                            <b>Company:</b> {{$product->carModel->car->company->name}}
                                        </div>
                                        <div>
                                            <b>Car:</b> {{$product->carModel->car->name}}
                                        </div>
                                        <div>
                                            <b>Model:</b> {{$product->carModel->model}}
                                        </div>
                                        <div>
                                            <b>Product Brand:</b> {{$product->company}}
                                        </div>
                                        @if($product->mrp == $product->price)
                                        <div>
                                            <b>MRP:</b> {{$product->price}}
                                        </div>
                                        @else
                                        <div>
                                            <b>MRP:</b> {{$product->mrp}}
                                        </div>
                                        <div>
                                            <b>Discounted Price:</b> {{$product->price}}
                                        </div>
                                        @endif
                                        <div>
                                            <b>Genuine Product:</b> @if($product->genuine_part == 1) Yes @else No @endif
                                        </div>
                                        <div>
                                            <b>Installation Charges(Fitting):</b> {{$product->installation_price}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                       if (!empty($cart_details)) {
                            $key = array_search($product->id, array_column($cart_details, 'productId'));
                        }
                        if (isset($key) && $key !== false) {
                            ?>
                            <button class="buy fl_right add_to_cart add_to_cart_selected" disabled="disabled">
                                Added
                            </button>
                        <?php } else { ?>
                            <button class="buy fl_right add_to_cart add_to_cart_selected-{{$product->id}}" data-product_price="{{$product->price}}" data-installation_charges="{{$product->installation_price}}" onclick="addToCart({{$product->id}})">
                                Add to Cart
                            </button>
                        <?php } ?>
                    </li>
                    @endforeach
                    @else
                    <li class="clear_fix">
                        <div class="fl_left table">
                            No parts found for current search. Try again.
                        </div>
                    </li>
                    @endif
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

