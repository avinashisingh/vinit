@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link rel="stylesheet" href="{{asset('css/frontend/owlcarousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('css/frontend/owlcarousel/owl.theme.default.min.css')}}">
<link href="{{ asset('css/frontend/home.css') }}" rel="stylesheet">
<link href="{{ asset('css/frontend/show_product.css') }}" rel="stylesheet">
<div class="row show-cart-images-row">
    <div class="col-md-5" id="show-cart-images">
        <div class="append-selected-main-img">
            <img id="main-image" class="image-responsive" src="{{ asset('productImages/'.$product->image) }}" alt="{{$product->name??""}}" data-zoom-image="{{ asset('productImages/'.$product->image) }}">
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
            <div id="preview" onmousemove="zoomIn(event)"></div>
        </div>

        <div class="main-owl">
            <div class="owl-carousel owl-theme" style="float: left; display: block !important;" id="owl-small">
                <div class="outer-img">
                    <a href="#">
                        <img src="{{asset('productImages/'.$product->image)}}">
                    </a>
                </div>
                <?php
                if (count($product->images) > 0) {
                    foreach ($product->images as $image) {
                        ?>
                        <div class="outer-img">
                            <a href="#">
                                <img src="{{asset('productImages/'.$image->image)}}">
                            </a>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-7" style="background-color: white; max-height: 335px;" id="product-info">
        <h1 class="">{{$product->name}}</h1>
        <p class=""><b>Brand:</b> {{$product->carModel->car->company->name}}</p>
        @if($product->mrp == $product->price)
            <p class=""><b>MRP:</b> &#8377; {{$product->price}}</p>
        @else
            <p class=""><b>MRP:</b> &#8377; {{$product->mrp}}</p>
            <p class=""><b>Discounted Price:</b> &#8377; {{$product->price}}</p>
        @endif
        <p class="genuine_part">
            <b style="">Genuine Part:</b> {{ ($product->genuine_part == 1) ?"Yes":"No" }} 
        </p>
        <p>
            <i class="fa fa-truck"></i> <i style="">Dispatch within 1 day</i>
        </p>
        <?php if ($product->installation_price != null) { ?>
            <p class="installation_price"><i class="fa fa-cogs"></i>
                <b style=""> Installation Price(Fitting):</b> &#8377; {{ $product->installation_price ??"" }} </p>
        <?php } ?>
        <div >
            <?php
            if(Session::has('cart')) {
            $cart_details = Session::get('cart');
            $key = array_search($product->id, array_column($cart_details, 'productId'));?>
            
            @if($key === false)
            <button type="submit" style="background-color: #17a2b8 !important;    border-color: transparent !important;" class="button btn btn-success pull-right add_to_cart_selected-{{$product->id}}"
                    data-product_price="{{$product->price}}" data-installation_charges="{{$product->installation_price}}"
                    onclick="addToCart({{$product->id}})">Add To Cart</button>
            @else
            <button style="background-color: #17a2b8 !important;    border-color: transparent !important;" class="button btn btn-success disabled pull-right">Added</button>
            @endif
            
            <?php } else {?>
            <button type="submit" style="background-color: #17a2b8 !important;    border-color: transparent !important;" class="button btn btn-success pull-right add_to_cart_selected-{{$product->id}}"
                    data-product_price="{{$product->price}}" data-installation_charges="{{$product->installation_price}}"
                    onclick="addToCart({{$product->id}})">Add To Cart</button>
            <?php } ?>
        </div>
    </div>
</div>
@endsection
@push('after-scripts')
<script>
    $(document).ready(function () {
//    $("#show-cart-images #owl-small").owlCarousel({
//    responsiveClass: true,
//            center: true,
//            autoHeight: false,
//            responsive: {
//            0: {
//            items: 1,
//                    nav: true
//            },
//                    600: {
//                    items: 1,
//                            nav: false
//                    },
//                    1000: {
//                    items: 2,
//                            nav: true,
//                    }
//            }
//    });

    $('.owl-carousel').owlCarousel({
    responsiveClass:true,
            nav: true,
            responsive:{
            0:{
            items:1,
            },
                    600:{
                    items:2,
                    },
                    1000:{
                    items:2,
                    }
            }
    })
            $("#show-cart-images a").on('click', function () {
    var image = $(this).find('img').attr('src');
    $('#show-cart-images .append-selected-main-img img').attr('src', image);
    });
    });
</script>

@endpush