@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.contact.box_title'))
<style>
    .card {
        margin: 0 0 25px 0;
    }
    .card-heading{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 0;
        padding-left: 20px;
    }
    .card-heading-con{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 0;
        padding-bottom: 15px;
        text-transform: capitalize;
        border-top: 1px solid rgba(0, 0, 0, 0.125);
        padding-top: 15px;
    }
    .contact-box{
        width: 100%;
        margin-bottom: 30px;
    }
    .contact-box img{
        width: 100%;
    }
</style>
@section('content')
<div class="row justify-content-center">
    <div class="container">
        <div class="col-sm-12">

        </div>
    </div>

    <div class="col col-sm-8 align-self-center">
        <div class="card">
            <h2 class="card-heading">
                @lang('labels.frontend.contact.box_title')
            </h2>
            <div class="card-body">
                {{ html()->form('POST', route('frontend.contact.send'))->open() }}
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('name', optional(auth()->user())->name)
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.name'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->email('email', optional(auth()->user())->email)
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('phone')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.phone'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->textarea('message')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.message'))
                                        ->attribute('rows', 6) }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0 clearfix">
                            {{ form_submit('Contact Now') }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                {{ html()->form()->close() }}


                <div class="contact-box">
                    <h3 class="card-heading-con">Find us here</h3>
                    <img alt="Contact Map" src="{{asset('img/frontend/contact-map.jpg')}}" >
                </div> 

            </div><!--card-body-->
        </div><!--card-->
    </div><!--col-->
</div><!--row-->
@endsection
