@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php dd('pok'); ?>
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">

<div id="c_step_3" class="check-panel pristine active">
    <div class="check-panel-title btn-change-event" data-event="event" data-autoportal-category="Boodmo" data-autoportal-action="Cart - Review Order" data-autoportal-label="Change">
        <div class="table">
            <div class="table-title">
                <i class="icon-check-o"></i>
                <span class="title-content">3. Review Order</span>
            </div>
            <div class=""><span class="forms-content">GrandTotal: 2 item(s)
                    <span class="icon-inr"></span> 600.00</span></div>
            <!--&#8377; &ndash;&gt; ₹-->
            <div>
                <button class="btn btn-change btn-change-event">
                    <span class="mobile_hide">change</span>
                    <i class="desktop_hide icon-retweet"></i>
                </button>            </div>
        </div>
    </div>
    <div class="check-panel-body">    <div class="order-wrapper"> <!-- repeat this block -->
            <div class="pull-right delivery-info_order-wrap text-center">
                <div class="delivery-info_order">
                    <i class="icon-truck-with-clock delivery-icon_order"></i> IMPORTANT: <strong>Estimated delivery date: 04-11-2018</strong>
                </div>
            </div>
            <strong>Order Package #1</strong>

            <div>
                <div class="pull-left">Sold by: TA (Rajasthan)</div>
            </div>

            <div class="order-body">
                <div class="order-labels mobile_hide">
                    <div>Item</div>
                    <div class="wd-15">Unit Price</div>
                    <div class="wd-10">Quantity</div>
                    <div class="wd-15">Amount</div>
                    <div class="wd-10">Total Delivery</div>
                    <div class="wd-15">Total</div>
                </div>
                <div>
                    <div class="clear_fix" data-name="description">
                        <div class="order-item-description relative">
                            <span class="best-offer">Best Offer</span>
                            <img src="https://boodmo.com/media/cache/catalog_list/images/parts/6c49821.jpg" alt="ASSY CABLE TRUNK LID LATCH-RHD">                            <div>
                                <span>
                                    <a href="/catalog/part-assy_cable_trunk_lid_latch_rhd-6615780/" target="_blank">
                                        ASSY CABLE TRUNK LID LATCH-RHD                                    </a>
                                </span>
                                <span>Part Number: 287481100113</span>
                                <span>Brand: TATA</span>
                            </div>
                        </div>
                    </div>
                    <div class="clear_fix" data-name="price">
                        <div class="desktop_hide"><b>Unit Price</b></div>
                        <div>
                            <span class="icon-inr"></span> 300.00                        </div>
                    </div>
                    <div class="clear_fix" data-name="quantity">
                        <div class="desktop_hide"><b>Quantity</b></div>
                        <div>2</div>
                    </div>
                    <div class="clear_fix" data-name="amount">
                        <div class="desktop_hide"><b>Amount</b></div>
                        <div>
                            <span class="icon-inr"></span> 600.00                        </div>
                    </div>
                    <div class="clear_fix" data-name="delivery">
                        <div class="desktop_hide"><b>Total Delivery</b></div>
                        <div>
                            <span class="icon-inr"></span> 0.00                        </div>
                    </div>
                    <div class="clear_fix" data-name="total">
                        <div class="desktop_hide"><b>Total</b></div>
                        <div>
                            <span class="icon-inr"></span> 600.00                        </div>
                    </div>
                </div>
            </div>

            <div class="order-footer">
                <div>
                    <div>
                        Package
                        total: <span class="icon-inr"></span> 600.00                </div>
                </div>
            </div>

        </div>


        <div class="pull-right">
            <div class="order-panel-footer">
                <div>
                    2 item(s)
                    <span class="icon-inr"></span> 600.00        </div>
                <div>
                    Delivery charge:
                    <span class="icon-inr"></span> 0.00        </div>
                <div>
                    <div>
                        Grand Total:
                        <span class="icon-inr"></span> 600.00            </div>
                </div>
            </div>
        </div>

        <div class="pull-left-md">
            <button class="btn big-btn button-blue btn-block">continue</button></div>
    </div>
</div>

@endsection

