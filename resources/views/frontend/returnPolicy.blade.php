@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php // dd('return'); ?>
<div>
    <div class="messages">
    </div>        
    <div class="content_block text">
        <h1 style="text-align: center;"><span style="color:#000000;"><span style="font-size:22px;"><u><strong>Return Policy</strong></u></span></span></h1>

        <p><span style="color:#000000;">Please, read this first before initiating a return. Use following email for any return related queries:<strong> Vinitgarg535@gmail.com&nbsp;</strong></span></p>

        <h2><span style="color:#000000;"><strong>Returns</strong> will be accepted only when following conditions are met:</span></h2>

        <p style="margin-left: 40px;"><span style="color:#000000;">*&nbsp;<strong>package</strong>&nbsp;is returned in original condition - <strong>not damaged,</strong> torn, oiled or compressed.</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">*&nbsp;<strong>product is unused</strong>&nbsp;and in the same condition as it was at the time of receipt (with no sign of installation or grease).</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">* request initiated<strong> in 10 days </strong>after products are delivered.</span></p>

        <h3><span style="color:#000000;"><strong>Exemptions</strong> from the policy:</span></h3>

        <p style="margin-left: 40px;"><span style="color:#000000;">* Items which belong to following categories are not subject to refund: electronic and complicated electric&nbsp;parts (harnesses, instrument panels, parts with circuit boards&nbsp;etc.)</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">* Vendors, merchant can have their own&nbsp;requirements or refund policy.</span></p>

        <h2><span style="color:#000000;"><strong>Return procedure</strong></span></h2>

        <p style="margin-left: 40px;"><span style="color:#000000;">You can initiate the return or replace from you customer panel in vinitautomobile.in</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">Customer shall check the consignment for any Physical damage and, if any found, mention&nbsp;this in POD (Proof of Delivery) documents of Logistics.</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">We recommend making photos of packages recieved and&nbsp;shooting a video of unpacking the product. In case of any problem with product share it with&nbsp;Vinitgarg535@gmail.com</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">In the case of item’s fault claim, the decision will be taken after inspection done by the manufacturer or certified organization.</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">Depending on the route and logistics services provider delivery&nbsp;time may&nbsp;vary.&nbsp;We can not guarantee that we will receive your returned item.</span></p>

        <p style="text-align: justify; margin-left: 40px;"><span style="color:#000000;">If you are shipping an item over Rs. 3500, you should consider using a trackable shipping service or/and purchasing shipping insurance.</span></p>

        <p style="text-align: justify; margin-left: 40px;"><span style="color:#000000;">Shipping and finance (gateway) costs are not subject to refund. The cost of return shipping can be&nbsp;deducted from your refund.</span></p>

        <p style="text-align: justify; margin-left: 40px;"><span style="color:#000000;">If return claim was rejected, we will send it back on&nbsp;customer's request or scrap after 30 days of keeping at stock.&nbsp;Customer will be responsible for paying all related shipping costs.</span></p>

        <h2 style="text-align: justify;"><span style="color:#000000;"><strong>Refunds</strong></span></h2>

        <p style="margin-left: 40px;"><span style="color:#000000;">When return is received, inspected and&nbsp;approved, your refund will be processed during 1 business day.</span></p>

        <p style="margin-left: 40px;"><span style="color:#000000;">COD orders:&nbsp;<em>refund will be done only to the bank account (debit/credit card). Documents proving the ownership of account has to be provided by&nbsp;</em><em>customer</em><em>&nbsp;for processing of&nbsp;</em><em>refund</em><em>.&nbsp;</em></span></p>

        <ul>
        </ul>
    </div>
</div>
@endsection