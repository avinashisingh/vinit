{{ html()->modelForm($logged_in_user, 'PATCH', route('frontend.user.profile.update'))->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
<div class="row">
    <div class="col">
        <div class="form-group">

            {{ html()->text('first_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.first_name'))
                    ->attribute('maxlength', 191)
                    ->required()
                    ->autofocus() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">

            {{ html()->text('last_name')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.last_name'))
                    ->attribute('maxlength', 191)
                    ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

@if ($logged_in_user->canChangeEmail())
<div class="row">
    <div class="col">
        <div class="alert alert-info">
            <i class="fas fa-info-circle"></i> @lang('strings.frontend.user.change_email_notice')
        </div>

        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

            {{ html()->email('email')
                        ->class('form-control')
                        ->placeholder(__('validation.attributes.frontend.email'))
                        ->attribute('maxlength', 191)
                        ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->
@endif

<div class="row">
    <div class="col">
        <div class="form-group mb-0 clearfix">
            <button class="btn btn-sm pull-right" style="background-color: #16c7f6 !important; color:white;" type="submit">Update</button>
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->
{{ html()->closeModelForm() }}

@push('after-scripts')
<script>
    $(function () {
        var avatar_location = $("#avatar_location");

        if ($('input[name=avatar_type]:checked').val() === 'storage') {
            avatar_location.show();
        } else {
            avatar_location.hide();
        }

        $('input[name=avatar_type]').change(function () {
            if ($(this).val() === 'storage') {
                avatar_location.show();
            } else {
                avatar_location.hide();
            }
        });
    });
</script>
@endpush
