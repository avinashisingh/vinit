<div class="table-responsive">
    <table class="table table-bordered">
        <tr>
            <th>@lang('labels.frontend.user.profile.name')</th>
            <td>{{ $logged_in_user->name }}</td>
        </tr>
        <tr>
            <th>@lang('labels.frontend.user.profile.email')</th>
            <td>{{ $logged_in_user->email }}</td>
        </tr>
        <tr>
            <th>Phone Number</th>
            <td>{{ $logged_in_user->phone_number }}</td>
        </tr>
    </table>
</div>
