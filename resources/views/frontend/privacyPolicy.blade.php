@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php // dd('privacy'); ?>
<div>
    <div class="messages">
    </div>        
    <p style="text-align: center;">&nbsp;</p>

    <h1><em><strong>Privacy Policy - Vinit Automobiles</strong></em></h1>

    <p>&nbsp;</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">This Privacy Policy is effective as of December 01, 2018. This is our official Privacy Policy which forms part of your legal agreement with us. Please read our Privacy Policy carefully to be sure you understand how we collect and use information and under what circumstances we share information.</p>

    <p style="text-align: justify;">By accessing the services provided by the site service vinitautomobile.in (“Website”), You agree to the collection and use of your data by the site/service vinitautomobile.in in the manner provided in this Privacy Policy. This Policy does not apply to the procedures and practices followed by entities that are not managed, owned or controlled by the Company and the site/service vinitautomobile.in or to the people that are not engaged, employed or managed by the Company and the site/service vinitautomobile.in. Our objective is to make you:</p>

    <p style="text-align: justify;">* feel comfortable using our Websites;</p>

    <p style="text-align: justify;">* feel secure in submitting your information to us;</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>WHAT INFORMATION DO WE COLLECT ABOUT YOU?</strong></p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">We collect information from you when you sign up on the Website. When registering on the Website, you may be asked to enter your personal information (like your name, gender, age, language preferences, the location of residence, mobile number, etc).</p>

    <p style="text-align: justify;">We will automatically receive and collect certain anonymous information in standard usage logs through our Web server, including computer-identification information obtained from "cookies," sent to your browser from:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* web server cookie stored on your hard drive;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* an IP address, assigned to the computer which you use;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* the domain server through which you access our service;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* the type of computer you're using.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">We may also collect information from the links you click on our Website and the number of times you access the page.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; * If you purchase a product or service from us, we request certain personally identifiable information from you on our order form. You must provide contact information (such as name, email, and shipping address) and financial information (such as credit card number, expiration date, CVV verification, Name on card, billing address etc.). We use this information for billing purposes and to process your orders. If we have trouble processing an order, we will use this information to contact you.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; * We will collect personally identifiable information about you only as part of a voluntary registration process, on-line survey, or contest or any combination thereof.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Our advertisers may collect anonymous traffic information from their own assigned cookies to your browser.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* The Site contains links to other websites. We are not responsible for the privacy practices of such websites which we do not own, manage or control.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; * We make chat rooms, forums, instant messenger and message boards and other services available to you. Please understand that any information that is disclosed in these areas becomes public information. We have no control over its use and you should exercise caution when disclosing your personal information to anyone.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; *&nbsp;If you use a bulletin board or chat room on this site, you should be aware that any personally identifiable information you submit there can be read, collected, or used by other users of these forums, and could be used to send you unsolicited messages. We are not responsible for the personally identifiable information you choose to submit in these forums.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; * We are the sole owners of your information collected by us at several different points on our Website.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">Please&nbsp;note that the above-listed information collected by us may be stored in our database for future reference.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>HOW WE USE THIS INFORMATION?</strong></p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* We use your email address to send you:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Password reminder and registration confirmation;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Special offers;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Newsletters;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Changes in the service's policy or terms of use;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* Event based communications such as order information, renewal notices, invites, reminders etc.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>Note:</strong> We send users newsletters and updates upon registration. We send newsletters and/or promotional emails on behalf of our alliance partners.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">We use your personal information to:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* help us provide personalised features;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* tailor our Website to your interest;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* to get in touch with you in the case of password retrieval and policy changes;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* to provide the services requested by you;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* to preserve social history as governed by existing law or policy</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">We use contact information internally to:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* direct our efforts for product improvement;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* contact you as a survey respondent;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* notify you if you win any contest; and</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* send you promotional materials from our contest sponsors or advertisers.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">Generally, we use anonymous traffic information to:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* remind us of who you are in order to deliver to you a better and more personalised service from both an advertising and an editorial perspective;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;*&nbsp;recognise your access privileges to our Websites;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* help diagnose problems with our server;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* administer our Websites;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp;* track your session so that we can understand better how people use our Website.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>WHOM DO WE DISCLOSE YOUR INFORMATION?</strong></p>

    <p style="text-align: justify;">We will not use your financial information for any purpose other than to complete a transaction with you.</p>

    <p style="text-align: justify;">We do not share or rent your personal information with third parties except our agents and alliance partners.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>FDMSS</strong></p>

    <p style="text-align: justify;">We use the services of our enabling partners such as outside shipping companies, resellers, business associates to fulfil orders, and credit card processing companies to process the payment for goods and services ordered from the website vinitautomobile.in. These entities do not retain, store, share or use personally identifiable information for any other purposes.</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; *&nbsp;<em>All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.&nbsp;</em></p>

    <p><em>&nbsp; &nbsp; &nbsp; * The Website Policies and Terms &amp; Conditions may be changed or updated occasionally to meet the requirements and standards. Therefore the Customers’ are encouraged to frequently visit these sections in order to be updated about the changes on the website. Modifications will be effective on the day they are posted.</em></p>

    <p><em>&nbsp; &nbsp; &nbsp; * Some of the advertisements you see on the Site are selected and delivered by third parties, such as ad networks, advertising agencies, advertisers, and audience segment providers. These third parties may collect information about you and your online activities, either on the Site or on other websites, through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to you advertisements that are tailored to your interests. Please remember that we do not have access to, or control over, the information these third parties may collect. The information practices of these third parties are not covered by this privacy policy.</em></p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>ALLIANCE PARTNERS</strong></p>

    <p style="text-align: justify;">We will share your information with our Alliance Partners who work with us or on our behalf to help provide you with the services. An alliance partner is a company or an individual who owns and manages (wholly or part of) online goods/ services on their websites powered by vinitautomobile.in.</p>

    <p style="text-align: justify;">We share email addresses with Agents and Alliance Partners. The Agents and Alliance Partners use the email address to confirm the deliveries, send notices and offer services related to the goods/ service.</p>

    <p style="text-align: justify;">We do not rent, sell or share your personal information and we will not disclose any of your personally identifiable information to third parties unless:</p>

    <p style="text-align: justify;">we have your permission:</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; * to provide products or services you've requested;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; * to help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud, potential threat to the safety or security of any person, violations of vinitautomobile.in terms of use or to defend against legal claims;</p>

    <p style="text-align: justify;">&nbsp; &nbsp; &nbsp; &nbsp; * special circumstances such as compliance with subpoenas, court orders, requests/order from legal authorities or law enforcement agencies requiring such disclosure.</p>

    <p style="text-align: justify;">We reserve the right to disclose your personally identifiable information and email address as required by law and when we believe that disclosure is necessary to protect our rights and/or comply with a judicial proceeding, court order, or legal process served on our Web site.</p>

    <p style="text-align: justify;">We share your information with advertisers on an aggregate basis only.</p>

    <p style="text-align: justify;">The security of your personal information and email address is important to us. When you enter sensitive information (such as credit card number and/or social security number) on our registration or order forms, we encrypt that information using secure socket layer technology (SSL). To learn more about SSL, follow this link//www.verisign.com/ssl/index.html. We follow generally accepted industry standards to protect the personal information and email address submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however. Therefore, while we strive to use commercially acceptable means to protect your personal information and email address, we cannot guarantee its absolute security.</p>

    <p style="text-align: justify;">If you have any questions about security on our Website, you can send an email.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>WHAT CHOICES ARE AVAILABLE TO YOU REGARDING COLLECTION, USE AND DISTRIBUTION OF YOUR INFORMATION?</strong></p>

    <p style="text-align: justify;">Supplying personally identifiable information is entirely voluntary. You are not required to register with us in order to use our Website. However, we offer some services only to visitors who do register.</p>

    <p style="text-align: justify;">You may change your interests at any time and may opt-in or opt-out of any marketing / promotional / newsletters mailings. vinitautomobile.in reserves the right to send you certain service related communication, considered to be a part of your vinitautomobile.in account without offering you the facility to opt-out. You may update your information and change your account settings at any time.</p>

    <p style="text-align: justify;">If you no longer wish to receive our newsletter and promotional communications, you may opt-out of receiving them by following the instructions included in each newsletter or communication or by emailing us.</p>

    <p style="text-align: justify;">We provide you with the opportunity to 'opt-out' of having your email address used for certain purposes when we ask for this information. For example, if you purchase a product/service but do not wish to receive any additional marketing material from us, you can indicate your preference, follow the instructions included in each newsletter or communication or by emailing us.</p>

    <p style="text-align: justify;">Upon request, we will remove/block your personally identifiable information from our database, thereby cancelling your registration.</p>

    <p style="text-align: justify;">However, your information may remain stored in archive on our servers even after the deletion or the termination of your account.</p>

    <p style="text-align: justify;">If your personally identifiable information or email address changes, or if you no longer desire our service, you may correct, update or deactivate it by making the change on our member information page or by emailing our Customer Support at Feedback or by contacting our contact information listed below.</p>

    <p style="text-align: justify;">If we plan to use your personally identifiable information for any commercial purposes, we will notify you at the time we collect that information and allow you to opt-out of having your information used for those purposes.</p>

    <p style="text-align: justify;">You can accept or decline the cookies. All sites that are customizable require that you accept cookies. You also must accept cookies to register as someone for access to some of our services. For information on how to set your browser to alert you to cookies, or to reject cookies, go to //www.cookiecentral.com/faq/</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>WHAT DO WE DO TO KEEP YOUR INFORMATION SECURE?</strong></p>

    <p style="text-align: justify;">The company has implemented reasonable physical, technical and administrative measures to safeguard the information we collect in connection with the services. However, please note that although we take reasonable steps to protect your information, no website, Internet transmission, computer system or wireless connection is completely secure.</p>

    <p style="text-align: justify;">As you may appreciate, the internet is a public network and we cannot guarantee that communications between you and Company or others to, through or from the Website, will be free from unauthorised access or interference by third parties. By using the Website, you are agreeing to assume this risk and any and all responsibility and liability that may arise. We have put in place reasonable procedures to help safeguard information.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;"><strong>CONSENT, AMENDMENTS AND GOVERNING LAW</strong></p>

    <p style="text-align: justify;">By using this Website, you consent to the terms of this Privacy Policy and to our use and management of Personal Information. Should a change be incorporated in this Privacy Policy, we will make all reasonable effort to inform you of the same.</p>

    <p style="text-align: justify;">Your visit and any dispute over privacy are subject to this Privacy Policy. The said Policy shall be governed by and construed in accordance with the laws of the Republic of India. Further, it is irrevocably and unconditionally agreed that the courts of Gurgaon, India shall have exclusive jurisdiction to entertain any proceedings in relation to any disputes arising out of the same.</p>

    <p style="text-align: justify;">&nbsp;</p>

</div>
@endsection
