@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

<style>
    .profile_information input {
        width: 100%;
        padding: 5px 0 5px 10px;
        border: 1px solid #969696;
    }
    .profile_information {
        margin-bottom: 40px;
    }
    .profile_information .info_label {
        float: left;
        width: 100%;
        line-height: 1.5;
    }
    .main-single-info-block {
        float: left;
        width: 100%;
        margin: 10px 0 10px 0;
    }
    .main-single-info-block button {
        padding: 4px 10px;
    }
    .info_store_btn {
        margin-bottom: 60px;
    }
    .info_store_btn button {
        padding: 10px 20px;
        background: #00adeb;
        border-radius: 5px;
    }
</style>
@section('content')
<div class="row" >
    <div class="col-md-6 profile_information" id="personal-information">
        <h3>Personal Information</h3>
        <div class="main-single-info-block">
            <div class="col-md-3 info_label">
                <b>First Name</b>
            </div>
            <div class="col-md-9 info_label">
                <input type="text" value="">
            </div>
        </div>
        <div class="main-single-info-block">
            <div class="col-md-3 info_label">
                <b>Last Name</b>
            </div>
            <div class="col-md-9 info_label">
                <input type="text" value="">
            </div>
        </div>
        <div class="main-single-info-block">
            <div class="col-md-3 info_label">
                <b>Phone Number</b>
            </div>
            <div class="col-md-9 info_label">
                <input type="number" value="" disabled="disabled">
            </div>
        </div>
        <div class="main-single-info-block">
            <div class="col-md-3 info_label">
                <b>Email</b>
            </div>
            <div class="col-md-9 info_label">
                <input type="text" value="" disabled="disabled">
            </div>
        </div>
        <div class="main-single-info-block">
            <div class="col-md-3 info_label">
                <b>Password</b>
            </div>
            <div class="col-md-9 info_label">
                <a href="{{ route('frontend.auth.password.email') }}" class="button btn btn-success">CHANGE</a>
            </div>
        </div>
    </div>
</div>
<div class="Info_store_btn pull-right">
    <button class="button btn-info">Save</button>
</div>
@endsection