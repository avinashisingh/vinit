@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<?php // dd('terms'); ?>
<div>
    <div class="messages">
    </div>        
    <h1 style="text-align: center;"><u><strong>WEBSITE TERMS AND CONDITIONS OF USE</strong></u></h1>

    <p>&nbsp;</p>

    <p><strong>1. &nbsp;INTRODUCTION</strong></p>

    <p>vinitautomobile.in ("Website") is a web-portal, owned and operated by <a href="http://leagueofclicks.com/"> LeagueOfClicks.</a></p>

    <p style="text-align: justify;">&nbsp;</p>

    <p>&nbsp;</p>

    <p><strong>2. &nbsp;ACCEPTANCE OF TERMS OF USE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">vinitautomobile.in provides data for the customer to search required spare parts or car-related product, any information regarding cars, their maintenance etc. (referred to hereafter as "Service") subject to the following terms and conditions of use ("Terms of Use"). By using the Service in any way, you are agreeing to comply with these Terms of Use. In addition, when using particular vinitautomobile.in services, you agree to abide by any applicable posted guidelines for all vinitautomobile.in Services, which are subject to change.</p>

    <p style="text-align: justify;">Should you object to any Terms of Use, any guidelines, or any subsequent modifications thereto or become dissatisfied with vinitautomobile.in in any way, your only recourse is to immediately discontinue use of vinitautomobile.in. You are, however, encouraged to inform/complain to our customer care team who will do their best to address your concerns.</p>

    <p>&nbsp;</p>

    <p><strong>3. &nbsp;DESCRIPTION OF WEBSITE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">The aim of vinitautomobile.in is to organise market of spare parts in India and become the&nbsp;most sought after&nbsp;destination for everybody interested in car service industry wherein every user will have the possibility to&nbsp;identify the necessary part in an easy way through continuously updated catalogues.</p>

    <p style="text-align: justify;">The Website will provide information about applicability, replacement of spare parts or other car related products.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>4. &nbsp;GENERAL TERMS OF USE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">i. &nbsp;User/ customer hereby impliedly agrees to be bound by this Terms of Use by using the Website, whether such usage is for the purpose of any purchase or not;</p>

    <p style="text-align: justify;">ii. User/ customer is hereby authorised to use the Website for lawful purposes or for the purposes as mentioned in the Terms of Use;</p>

    <p style="text-align: justify;">iii. Any unauthorized use of the Website will lead to strict legal action against the user/ customer as per the applicable laws;</p>

    <p style="text-align: justify;">iv. The prices and availability of the products offered on the Website are subject to change, without any notice or any consequential liability on the Company;</p>

    <p style="text-align: justify;">v. All offers on products shall be for limited period of time, as mentioned under the order specification. Company may at its sole discretion modify, extend, update or withdraw the offers on products without prior notice to the users, customers but such modification will be intimated on the Website;</p>

    <p style="text-align: justify;">vi. Company may, at any time due to various reasons, including but not limited to technical errors, unavailability of stocks or for any other reasons whatsoever, cancel the orders placed by users/ customers. Company’s decision of cancellation of order shall be final and Company shall not be liable for such cancellation whatsoever. If the order is cancelled after credit card has been charged, the said amount will be reversed in user's credit card account;</p>

    <p style="text-align: justify;">vii. Company strives to provide accurate products, services and pricing information. In the event that a product or service is listed at an incorrect price or with incorrect information due to an error in pricing or product or service information, Company may, at its discretion, either contact user/ customer for instructions or cancel the user's/ customer’s order, which the user/ customer will be notified about;</p>

    <p style="text-align: justify;">viii. Company shall have right to modify the price of a product or service any time, without any prior information;</p>

    <p style="text-align: justify;">ix. In case of requests for order cancellations, Company, at its sole discretion, reserves the right to accept or reject requests for order cancellations for any reason whatsoever, without being liable to show cause to the user/ customer. As part of usual business practice, if Company receives a cancellation notice and the order has not been processed/ approved by Company, Company shall cancel the order and refund the entire amount to user/customer within a reasonable period of time;</p>

    <p style="text-align: justify;">x. In case an order has already been processed, it is pertinent to note that the Company will not be able to cancel the same and for the purpose has the full right to decide whether an order has been processed or not. User/ customer hereby agrees and undertakes that the decision taken by the Company is acceptable to user/ customer and user/ customer shall not create any dispute on the decision taken by Company on cancellation;</p>

    <p style="text-align: justify;">xi. Under no circumstances Company's liability shall exceed giving the user/customer a replacement of the same product/ alternate product of the same value;</p>

    <p style="text-align: justify;">xii. These terms and conditions are subject to Indian laws and any dispute shall be subject to jurisdiction of the courts in Chandigarh (India) only, and may be modified by Company any time, for which Company is not liable to notify the users/ customers.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>5. &nbsp;MODIFICATIONS TO THIS TERMS OF USE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">We reserve the right, at our sole discretion, to change, modify or otherwise alter these terms and conditions at any time. Such modifications shall become effective immediately upon the posting thereof. You must review this agreement on a regular basis to keep yourself apprised of any changes.</p>

    <p>&nbsp;</p>

    <p><strong>6. &nbsp;CONTENT</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">The content at vinitautomobile.in includes information provided and created by car dealers, advertisers, content partners, software developers, publishers, marketing agents, users, resellers and other third parties. While every attempt has been made to ascertain the authenticity of the content of the Website, The Company has no control over the accuracy of such information on our pages, and material on the vinitautomobile.in. Website may include technical inaccuracies or typographical errors, and we make no guarantees, nor can we be responsible for any such information, including its authenticity, currency, content, quality, copyright compliance or legality, or any resulting loss or damage. All of the data on services including but not limited to, the prices and the availability of any product or service or any feature thereof, is subject to change without notice to the party providing it. vinitautomobile.in reserves the right, in its sole discretion and without any obligation, to make improvements to, or correct any error or omissions in, any portion of the Website. Where appropriate, we will endeavor to update information listed on the Website on a timely basis, but shall not be liable for any inaccuracies.</p>

    <p>&nbsp;</p>

    <p><strong>7. &nbsp;OWNERSHIP OF RIGHTS</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">All rights, title and interest including trademarks and copyrights in respect of the domain name and site content hosted on the Website are reserved by the Company. Users are permitted to read, print or download text, data and/or graphics from the site for their personal use only. Unauthorized access, reproduction, redistribution, transmission and/or dealing with any information contained in this Website in any other manner, either in whole or in part, are strictly prohibited, failing which strict legal action will be initiated against such users. You understand that all postings, car dealer data, messages, text, files, images, photos, video, sounds, or other materials ("Content") posted on, transmitted through, or linked from the service, are the sole responsibility of the person from whom such Content originated. More specifically, you are entirely responsible for all content that you post, email or otherwise make available via the service. You may not decompile or disassemble, reverse engineer or otherwise attempt to discover any source code contained in the Website. You understand that Company does not entirely control, and is not responsible for Content made available through the service, and that by using the service, you may be exposed to Content that is offensive, indecent, inaccurate, misleading, or otherwise objectionable. Furthermore, the vinitautomobile.in site and Content available through the service may contain links to other websites, which are completely independent of vinitautomobile.in.</p>

    <p style="text-align: justify;">Although Company does not claim ownership of Content that its users post, by posting Content to any public area of the Website, you automatically grant, and you represent and warrant that you have the right to grant, to Company an irrevocable, perpetual, non-exclusive, fully paid, worldwide license to use, copy, perform, display, and distribute the said Content and to prepare derivative works of, or incorporate into other works, said Content, and to grant and authorize sublicenses (through multiple tiers) of the foregoing. Furthermore, by posting Content to any public area of the Website, you automatically grant Company all rights necessary to prohibit any subsequent aggregation, display, copying, duplication, reproduction, or exploitation of the Content on the Website by any party for any purpose.</p>

    <p style="text-align: justify;">All components of the Website are protected by intellectual property laws and are the property of the Company. Users shall not acquire any rights, including rights in or to any software, trademarks or components of the Website, by access or use of the Website. Copyright notices and trademarks may not be changed or removed. Components of the Website may not be reproduced in whole or in part in any manner or form (including electronic or printed form) without our prior written consent and unless full acknowledgement of the source is provided.</p>

    <p style="text-align: justify;">You and we acknowledge that, in the event of any third party claim that the Website infringes that third party’s intellectual property rights, we, will be solely responsible for the investigation, defence, settlement and discharge of any such intellectual property infringement claim; provided such infringement was not caused by us.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>8. &nbsp;DISCLAIMER OF WARRANTIES</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">The Website and the service are provided on an "as is" or "as available" basis, without any warranties of any kind. The Company makes no representation or warranty as to the accuracy, completeness or authenticity of the information contained in the Website. The Company makes no representation of MARUTI or MARUTI SUZUKI, HYUNDAI, TATA MOTORS, MAHINDRA, FIAT, HONDA, HINDUSTAN MOTORS, TOYOTA, NISSAN, General Motors, CHEVROLET, AUDI, BMW, VOLVO, JAGUAR, LAND ROVER, MERCEDES and other Car Vehicle Manufacturer / car brands or any other and its associate in any manner until clearly specify kind of association or agreement with associates directly or indirectly on the directory / web pages.</p>

    <p style="text-align: justify;">Your linking to any other website is at your own risk. You agree that you must evaluate, and bear all risks associated with, the use of any Content, that you may not rely on said Content, and that under no circumstances will the Company be liable in any way for any Content or for any loss or damage of any kind incurred as a result of the use of any Content posted, emailed or otherwise made available via the service. You acknowledge that vinitautomobile.in does not always pre-screen or approve Content (unless specified), but that vinitautomobile.in shall have the right (but not the obligation) in its sole discretion to refuse, delete or move any Content that is available via the service, for violating the letter or spirit of the Terms of Use or for any other reason.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>9. &nbsp;THIRD PARTY CONTENT, SITES, AND SERVICES</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">The vinitautomobile.in site and Content available through the service may contain features and functionalities that may link you or provide you with access to third party content which is completely independent of vinitautomobile.in, including web sites, directories, servers, networks, systems, information and databases, applications, software, programs, products or services, and the Internet as a whole. Your interactions with organizations and/or individuals found on or through the service, including payment and delivery of goods or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between you and such organizations and/or individuals. You should make whatever investigation you feel necessary or appropriate before proceeding with any online or offline communication/transaction with any of these third parties. You agree that the Company cannot be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings. If there is a dispute between participants on this Website, or between users and any third party, you understand and agree that the Company is under no obligation to become involved. In the event that you have a dispute with one or more other users, you hereby release Company, its officers, employees, agents and successors in rights from claims, demands and damages (actual and consequential) of every kind or nature, known or unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way related to such disputes and / or our service. Customer disputes with workshops/outlets/shops producers or suppliers providing information or goods to customers, also routed through vinitautomobile.in, shall be considered as THIRD PARTY.</p>

    <p style="text-align: justify;">vinitautomobile.in deals with producers and suppliers and offer their catalogue to customers as a mediator. We recommend taking professional advice from workshop specialist during choosing spare parts intended for installation on your car. In the case of any fitment, quality issue with car part supplier, Company does not take any responsibility for issues and quality of parts but may help the customer to resolve issue if possible with mutual agreements between customer and supplier.</p>

    <p style="text-align: justify;">CUSTOMER CAN NOT CLAIM from Company for car parts issues and other suppliers’ prices, discounts, quality and fitment.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>10.&nbsp;&nbsp;PRIVACY AND INFORMATION DISCLOSURE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">Company has established a Privacy Policy to explain to users how their information is collected and used, which is located at the following web address: https://vinitautomobile.in/pages/privacy-policy.html. Your use of the Website or the service signifies acknowledgement of and agreement to our Privacy Policy. You further acknowledge and agree that Company may, in its sole discretion, preserve or disclose your content, as well as your information, such as email addresses, IP addresses, timestamps, and other user information, if required to do so by law or in the good-faith belief that such preservation or disclosure is reasonably necessary to: comply with legal process; enforce these Terms of Use; respond to claims that any Content violates the rights of third-parties; respond to claims that contact information (e.g. phone number, address) of a third-party has been posted or transmitted without their consent or as a form of harassment; protect the rights, property, or personal safety of vinitautomobile.in, its users or the general public.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>11.&nbsp;&nbsp;CONDUCT</strong></p>

    <p>&nbsp;</p>

    <p>You agree not to post, email, or otherwise make available content:</p>

    <p style="text-align: justify;">a. that is unlawful, harmful, threatening, abusive, harassing, defamatory, libelous, invasive of another's privacy, or is harmful to minors in any way;</p>

    <p style="text-align: justify;">b. that is pornographic in nature;</p>

    <p style="text-align: justify;">c. that harasses, degrades, intimidates or is hateful toward an individual or group of individuals on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability;</p>

    <p style="text-align: justify;">d. that impersonates any person or entity or falsely states or otherwise misrepresents your affiliation with a person or entity;</p>

    <p style="text-align: justify;">e. that includes personal or identifying information about another person without that person's explicit consent;</p>

    <p style="text-align: justify;">f. that is false, deceptive, misleading, deceitful, or constitutes "bait-and-switch";</p>

    <p style="text-align: justify;">g. that infringes any patent, trademark, trade secret, copyright or other proprietary rights of any party, or Content that you do not have a right to make available under any law or under contractual or fiduciary relationships;</p>

    <p style="text-align: justify;">h. that constitutes or contains "affiliate marketing," "link referral code," "junk mail," "spam," "chain letters," "pyramid schemes," or unsolicited commercial advertisement;</p>

    <p style="text-align: justify;">i. that constitutes or contains any form of advertising or solicitation;</p>

    <p style="text-align: justify;">j. that includes links to commercial services or websites, except as allowed in "services";</p>

    <p style="text-align: justify;">k. that advertises any illegal service or the sale of any items the sale of which is prohibited or restricted by any applicable law, including without limitation items the sale of which is prohibited or regulated by Indian law.</p>

    <p style="text-align: justify;">l. that contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware or telecommunications equipment;</p>

    <p style="text-align: justify;">m. that disrupts the normal flow of dialogue with an excessive number of messages (flooding attack) to the service, or that otherwise negatively affects other users' ability to use the service; or</p>

    <p style="text-align: justify;">n. that employs misleading email addresses, or forged headers or otherwise manipulated identifiers in order to disguise the origin of Content transmitted through the service.</p>

    <p>Additionally, you agree not to:</p>

    <p>a. contact anyone who has asked not to be contacted;</p>

    <p>b. "stalk" or otherwise harass anyone;</p>

    <p style="text-align: justify;">c. collect personal data about other users for commercial or unlawful purposes;</p>

    <p style="text-align: justify;">d. use automated means, including spiders, robots, crawlers, data mining tools, or the like to download data from the Service - unless expressly permitted by vinitautomobile.in;</p>

    <p style="text-align: justify;">e. post non-local or otherwise irrelevant Content, repeatedly post the same or similar Content or otherwise impose an unreasonable or disproportionately large load on our infrastructure;</p>

    <p style="text-align: justify;">f. attempt to gain unauthorized access to vinitautomobile.in’s computer systems or engage in any activity that disrupts, diminishes the quality of, interferes with the performance of, or impairs the functionality of, the Service or the vinitautomobile.in website; or</p>

    <p style="text-align: justify;">g. use any form of automated device or computer program that enables the submission of postings on vinitautomobile.in without each posting being manually entered by the author thereof (an "automated posting device"), including without limitation, the use of any such automated posting device to submit postings in bulk, or for automatic submission of postings at regular intervals.</p>

    <p>&nbsp;</p>

    <p><strong>12.&nbsp;&nbsp;LIMITATIONS ON SERVICE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">You acknowledge that vinitautomobile.in may establish limits concerning use of the service, including the maximum number of days that Content will be retained by the service, the maximum number and size of postings, email messages, or other Content that may be transmitted or stored by the service, and the frequency with which you may access the service. You agree that Company has no responsibility or liability for the deletion or failure to store any Content maintained or transmitted by the service. You acknowledge that Company reserves the right at any time to modify or discontinue the service (or any part thereof) with or without notice, and that Company shall not be liable to you or to any third party for any modification, suspension or discontinuance of the service.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>13.&nbsp;&nbsp;ACCESS TO THE SERVICE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">Company grants you a limited, revocable, non-exclusive license to access the Website to avail the service for your own personal use. This license does not include any collection, aggregation, copying, duplication, display or derivative use of the service nor any use of data mining, robots, spiders, or similar data gathering and extraction tools for any purpose unless expressly permitted by the Company. A limited exception is provided to general purpose internet search engines and non-commercial public archives that use such tools to gather information for the sole purpose of displaying hyperlinks to the service, provided they each do so from a stable IP address or range of IP addresses using an easily identifiable agent and comply with our robots.txt file. "General purpose internet search engine" does not include a website or search engine or other service that specializes in classified listings or in any subset of classifieds listings such as jobs, housing, for sale, services, or personals, or which is in the business of providing classified ad listing services. Company permits you to display on your website, or create a hyperlink on your website to, individual postings on the service so long as such use is for non-commercial and/or news reporting purposes only (e.g., for use in personal web blogs or personal online media).</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>14.&nbsp;&nbsp;FINANCIAL DETAILS</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">User agrees, understands and confirms that the credit / debit card details or other financial details provided, for availing of services on the Website will be correct and accurate and user shall not use the credit /debit card or financial facility which is not lawfully owned / obtained by the user. User also understands that any financial information submitted by the user is directly received by Company’ acquiring bank and not taken by Company directly. Company will not be liable for any credit / debit card fraud. The liability for use of a card fraudulently will be on the user and the onus to 'prove otherwise' shall be exclusively on user. Company and its associated acquiring bank or financial institutions reserve the right to recover the cost of goods, collection charges and lawyers fees from persons using the Website fraudulently, initiate legal proceedings against such persons for fraudulent use of the Website and any other unlawful acts or acts or omissions in breach of these terms and conditions in accordance with applicable laws.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p><strong>15.&nbsp;&nbsp;TERMINATION OF SERVICE</strong></p>

    <p>&nbsp;</p>

    <p style="text-align: justify;">You agree that Company, in its sole discretion, has the right (but not the obligation) to delete or deactivate your account, block your email or IP address, or otherwise terminate your access to or use of the service (or any part thereof), immediately and without notice, and remove and discard any Content within the Service, for any reason, including, without limitation, if Company believes that you have acted inconsistently with the letter or spirit of the Terms of Use. Further, you agree that Company shall not be liable to you or any third-party for any termination of your access to the service. Further, you agree not to attempt to use the service after said termination. Sections 2, &nbsp;6, 12, 13, 15,-16 &amp; 17 shall survive termination of these Terms of Use.</p>

    <p style="text-align: justify;">&nbsp;</p>

    <p style="text-align: justify;">
    <style type="text/css">p.p2 {margin: 0.0px 0.0px 12.0px 48.0px; line-height: 14.0px; font: 12.0px Times; color: #000000; -webkit-text-stroke: #000000} p.p3 {margin: 0.0px 0.0px 12.0px 48.0px; text-align: justify; line-height: 14.0px; font: 12.0px Times; color: #000000; -webkit-text-stroke: #000000} li.li1 {margin: 0.0px 0.0px 0.0px 0.0px; line-height: 14.0px; font: 12.0px Times; color: #000000; -webkit-text-stroke: #000000} span.s1 {-webkit-text-stroke: 0px #000000} span.s2 {font-kerning: none} span.Apple-tab-span {white-space:pre} ol.ol1 {list-style-type: decimal}
    </style>
</p>

<p><b>16. </b><b>LIMITATIONS OF LIABILITY &amp; INDEMNITY</b></p>

<p>&nbsp;</p>

<p style="text-align: justify;">Under no circumstances shall Company be liable for direct, indirect, incidental, special, consequential or exemplary damages (even if Company has been advised of the possibility of such damages), resulting from any aspect of your use of the Website or the service, whether the damages arise from use or misuse of the Website or the service, from inability to use the Website or the service, or the interruption, suspension, modification, alteration, or termination of the Website or the service. Such limitation shall also apply with respect to damages incurred by reason of other services or products received through or advertised in connection with the Website or the service or any links on the Website, as well as by reason of any information or advice received through or advertised in connection with the Website or the service or any links on the Website. These limitations shall apply to the fullest extent permitted by law. In some jurisdictions, limitations of liability are not permitted. In such jurisdictions, some of the foregoing limitation may not apply to you. You agree to indemnify and hold the Company, its officers, subsidiaries, affiliates, successors, assigns, directors, officers, agents, service providers, suppliers and employees, harmless from any claim or demand, including reasonable attorney fees and court costs, made by any third party due to or arising out of Content you submit, post or make available through the service, your use of the service, your violation of the Terms of Use, your breach of any of the representations and warranties herein, or your violation of any rights of another.</p>

<p style="text-align: justify;">&nbsp;</p>

<p><strong>17.&nbsp;&nbsp;GENERAL INFORMATION</strong></p>

<p>&nbsp;</p>

<p style="text-align: justify;">The Terms of Use constitute the entire agreement between you and the Company and govern your use of the service, superseding any prior agreements between you and the Company. The Terms of Use and the relationship between you and Company shall be governed by the Indian laws without regard to its conflict of law provisions. You and Company agree to submit to the exclusive jurisdiction of the courts located in Gurgaon, India. The failure of Company to exercise or enforce any right or provision of the Terms of Use shall not constitute a waiver of such right or provision. If any provision of the Terms of Use is found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions of the Terms of Use remain in full force and effect. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the service or the Terms of Use must be filed within one (1) year after such claim or cause of action arose or be forever barred. It is entirely and sole decision of the Company to use any kind of promotion, channels of promotion to generate leads. Different way of (channel of) promotion may be any of the following mentioned - SMS campaign, pamphlets distribution, newspapers advertisement, yellow pages, distribution of coupons, online promotion, website exchange links and any other way of promotion. The promotion channel will be decided by Company for awareness and generation of leads. All users and suppliers agree to allow Company to use promotional material for generation of leads. Further explanation of this agreement, Company is doing exclusive promotion by creating directory / sub-domain web pages of car part supplier members to generate leads.</p>

<p style="text-align: justify;">&nbsp;</p>

<p><strong>18.&nbsp;&nbsp;DO-IT-YOURSELF AND CAR CARE TIPS OR ARTICLES</strong></p>

<p>&nbsp;</p>

<p style="text-align: justify;">The information contained in car care articles, tips, do-it-yourself guide, is for knowledge and educational purposes only and cannot substitute for the advice of professional car / auto mechanic or authorized car workshop or authorized car part dealer or service station. Please don't attempt to replace your car parts if you don't have proper knowledge and tools, you can be injured and your vehicle could be damaged. Take your car to dealer or a repair shop.</p>

<p>&nbsp;</p>

<p><strong>19.&nbsp;&nbsp;PICTURES / PHOTO USED IN THE WEBSITE</strong></p>

<p>&nbsp;</p>

<p style="text-align: justify;">Website content has pictures. The picture is representative of concept or service, not necessarily the original or true copy of product or service sold.</p>

<p>&nbsp;</p>

<p style="text-align: justify;"><strong>NOTE:</strong> We update these Terms from time-to-time, regularly and all customers/visitors at vinitautomobile.in agrees to read it when required and company will not inform separately for any updates of these Terms and Conditions.</p>

</div>
@endsection