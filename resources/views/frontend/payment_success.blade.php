@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">

<div id="payment-success-main">
    <div class="payment_success_left">
        <div class="success-icon">
            <i class="fa fa-check" aria-hidden="true"></i>
        </div>
        <h3>Thank you for your order.</h3>
        <p>#28/10/078628</p>
        <h4>308.00</h4>
        <p class="success-message">We are processing your payments, please wait for email with payment confirmation.</p>
    </div>
    <div class="payment_success_right">

        <h4>Shipping Address</h4>
        <p>Manpreet Singh</p>
        <p>B021-3698</p>
        <p>Chandigarh - 140301</p>
        <p><span>Chandigarh</span>, India</p>
        <p>Phone: 9876543210</p>
    </div>

</div>
@endsection

