@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

<style>
    .info_label {
        float: left;
        width: 100%;
        text-align: center;
    }
    .main-single-info-block {
        float: left;
        width: 100%;
        background: gainsboro;
        padding: 10px 0px;
    }
    .orders_list {
        float: left;
        width: 100%;
        padding: 10px 0;
        border-bottom: 1px solid gainsboro;
    }
    #my-orders {
        min-height: 450px;
        background: #fff;
        padding-left: 20px;
        padding-right: 20px;
        margin-bottom: 20px;
    }
    .card-heading{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 20px;
        padding-left: 5px;
    }
</style>
@section('content')
<?php // dd($orders); ?>
<div class="container" >
    <div class="col-md-12 my_orders card" id="my-orders">
        <h3 class="card-heading">My Orders</h3>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Order No</th>
                        <th>Bill Amount</th>
                        <th>Delivery Chargers</th>
                        <th>Total Payment</th>
                        <th>Address</th>
                        <th>Payment Status</th>
                        <th>Delivery Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($orders->count() > 0){ ?>
                    @foreach($orders as $key=>$order)
                    <tr class="each-order">
                        <td>{{ $key+1 }}</td>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->bill_amount }}</td>
                        <td>{{ $order->delivery_charges }}</td>
                        <td>{{ $order->bill_amount + $order->delivery_charges }}</td>
                        <td>{{ $order->address->address. ' '. $order->address->city->name. ' '. $order->address->city->state->name }}</td>
                        <td>{{Config::get('constant.payment.1')}}</td>
                        @if($order->delivery_status == Config::get('constant.inverse_delivery_status.pending'))
                        <td class="delivery-status">{{Config::get('constant.delivery_status.0')}}</td>
                        @else 
                        <td class="delivery-status">{{Config::get('constant.delivery_status.1')}}</td>
                        @endif
                        <td> 
                            <a href="{{route('frontend.user.myOrderDetails', $order->id)}}" title="Order Details" class="btn btn-sm btn-primary">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                    <?php } else { ?>
                    <tr><td colspan="8">No Orders till now.</td></tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    @endsection