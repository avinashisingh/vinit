@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">
<div id="c_step_1" class="check-panel active">
    <!--check-panel has status classes: pristine, active, dirty -->

    <div class="check-panel-title btn-change-event" data-event="event" data-autoportal-category="Boodmo" data-autoportal-action="Cart - Contact Information" data-autoportal-label="Change">
        <div class="table">
            <div class="table-title">
                <i class="icon-check-o"></i>
                <span class="title-content">1. <span class="title-content--dirty">Enter</span> Email</span>
            </div>
            <div class=""><span class="forms-content">abc@yopmail.com</span></div>
            <div>
                <button class="btn btn-change btn-change-event">
                    <span class="mobile_hide">change</span>
                    <i class="desktop_hide icon-retweet"></i>
                </button>                                    </div>
        </div>
    </div>

    <div class="check-panel-body check-panel-body-email">
        <div class="email-form-wrapper" style="display: inline-block">
            <form class="form-email billing-form" id="checkout-form-email" data-form-name="checkout-form-email" novalidate="novalidate">
                <label>
                    <div class="mb-10">Email Address</div>
                    <div>
                        <input type="text" id="billing-email-val" name="email" placeholder="Enter your email address" class="form-input valid" value="" aria-required="true" aria-invalid="false">
                    </div>
                    <div class="alert alert-danger error" id="email-error" style="display: none;"></div>
                </label>
                <div><input type="submit" value="continue" class="btn big-btn button-blue"></div>
            </form>                <form id="checkout-form-email-login" class="billing-form" data-form-name="checkout-form-email-login" novalidate="novalidate">
                <div>
                    <h3 style="display: inline-block"><b class="email-title"></b></h3>  <a href="javascript:void(0);" class="change-email">Change email</a>
                    <p class="sign-in-content">The above email is registered with us. Please enter <br> the password below and sign in to continue</p>
                </div>
                <div class="row">
                    <input type="hidden" name="identity" id="checkoutIdentity" class="identity" value="">
                    <input type="password" name="credential" id="checkoutCredential" placeholder="Password" class="form-input">
                </div>
                <div class="row">
                    <a href="javascript:void(0);" class="btn big-btn button-blue mr-10 -forgot-password" data-email="">Password recovery</a>
                    <input type="submit" class="btn big-btn button-blue" value="Sign in">
                </div>
            </form>
            <form method="post" id="form-forgot-password" data-form-name="form-forgot-password" action="/u/forgotpassword?return=WyJjaGVja291dFwvb25lcGFnZSJd" class="billing-form">
                <div>
                    <h3 style="display: inline-block"><b class="email-title"></b></h3>  <a href="javascript:void(0);" class="change-email">Change email</a>
                    <p class="sign-in-content">Forgot your password? click button below and <br> we will send you a link to recover your password</p>
                </div>
                <div class="row">
                    <input type="hidden" name="email" id="identity" class="identity" value="">
                </div>
                <div class="row">
                    <input type="submit" class="btn big-btn button-blue" value="Recover">
                </div>
            </form>
        </div>
    </div>

</div>
@endsection