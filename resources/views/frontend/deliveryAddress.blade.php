@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">
<?php
// dd($addresses); 
//unset($addresses);
?>
<div id="c_step_2" class="check-panel active">
    <div class="check-panel-title btn-change-event" data-event="event" data-autoportal-category="Boodmo" data-autoportal-action="Cart - Delivery Address" data-autoportal-label="Change">
        <div class="">
            <div class="table-title">
                <span class="title-content"><span class="title-content--dirty">Enter</span> Delivery Address</span>
            </div>
        </div>
    </div>
    <div class="check-panel-body relative">
        {{ html()->form('POST', route('frontend.user.storeAddress'))->open() }}
        <div class="form-input delivery-info">
            <div class="ico-warning">!</div> IMPORTANT: <strong>We deliver to Chandigarh / Tricity only!</strong>
        </div>
        <div class="relative">
            <div class="btn delivery-info-btn">i</div>
            <textarea name="address" cols="30" rows="3" placeholder="Address" class="form-input valid" 
                      aria-required="true" aria-invalid="false"><?php echo (isset($addresses)) ? $addresses[0]->address : ''; ?></textarea>
            <div class=" error" id="address-error" style="display: none;"></div>
        </div>
        <div>
            <select name="city" id="cities-name" class="form-input valid">
                <option value="">Select City</option>
                @foreach($cities as  $city)
                <option value="{{$city->id}}" data-state_id="{{$city->state_id}}" <?php echo ((isset($addresses) && $addresses[0]->city_id == $city->state_id) ? 'selected' : ''); ?> >{{$city->name}}</option>
                @endforeach
            </select>
        </div>
        <div class=" error" id="city-error" style="display: none;"></div>

        <div>
            <select name="state" id="states-name" class="form-input valid">
                <option value="">Select State</option>
                <?php if(isset($addresses) && !empty($addresses[0]->state)){ ?>
                <option value="{{$addresses[0]->state->id}}" selected >{{$addresses[0]->state->name}}</option>
                <?php } ?>
            </select>
        </div>
        <div class=" error" id="state-error" style="display: none;"></div>
        <div>
            <input name="pin" type="number" placeholder="Pincode" class="form-input form-delivery-input-pin valid" 
                   value="<?php echo (isset($addresses)) ? $addresses[0]->pincode : ''; ?>" pattern="[0-9]{6}" aria-required="true" aria-invalid="false">
            <label for="pin" id="pin-error" class="" style="display: none;"></label>
        </div>
        <div class="deliver-save">
            <input type="submit" value="save and continue" class="btn big-btn button-blue btn-block">
        </div>
        {{ html()->form()->close() }}

    </div>
</div>

@endsection

