@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

<style>
    .info_label {
        float: left;
        width: 100%;
        text-align: center;
    }
    .main-single-info-block {
        float: left;
        width: 100%;
        background: gainsboro;
        padding: 10px 0px;
    }
    .orders_list {
        float: left;
        width: 100%;
        padding: 10px 0;
        border-bottom: 1px solid gainsboro;
    }
    #my-orders {
        min-height: 450px;
        background: #fff;
        padding-left: 20px;
        padding-right: 20px;
        margin-bottom: 20px;
    }
    .card-heading{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 20px;
        padding-left: 5px;
    }
    #my-orders a {
        background-color: #16c7f6 !important;
        color:white;
        margin-bottom: 10px;
    }
</style>
@section('content')
<div class="container" >
    <div class="col-md-12 my_orders card" id="my-orders">
        <h3 class="card-heading">Orders details for order: #{{ $orderId }}</h3>
        <span class=""><a href="{{URL::previous()}}" class="button btn pull-right">Back</a></span>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Name</th>
                        <th>Product Company</th>
                        <th>Car Company</th>
                        <th>Car</th>
                        <th>Car Modal</th>
                        <th>Price</th>
                        <th>Installation Price</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $key=>$product)
                    <tr>
                        <td>{{ $key +1 }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->company }}</td>
                        <td>{{ $product->carModel->car->company->name }}</td>
                        <td>{{ $product->carModel->car->name }}</td>
                        <td>{{ $product->carModel->model }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->installation_price }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endsection