@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link href="{{ asset('css/frontend/show_cart.css') }}" rel="stylesheet">
<div class="thank-you-content">
    <div class="row">
        <div class="col-sm-12">
            <img src="{{asset('img/frontend/payment-success.png') }}" alt="" />
            <p>Thank you for your order. We are delivery you soon.</p>
        </div>
    </div>
</div>
@endsection

@push('after-scripts')

@endpush