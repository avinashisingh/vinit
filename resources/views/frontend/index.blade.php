@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
<link rel="stylesheet" href="{{asset('css/frontend/owlcarousel/owl.carousel.min.css')}}">
<link rel="stylesheet" href="{{asset('css/frontend/owlcarousel/owl.theme.default.min.css')}}">
<link href="{{ asset('css/frontend/home.css') }}" rel="stylesheet">
<div id="tiHero">
    <div id="tiHeroBanner">
        <div id="tiHeroHeader">Select Your Vehicle</div>
        <div id="search-main">
            {{ html()->form('GET', route('frontend.search'))->open() }}
            <div class="selection_content">
                <div class="ti-select-wrap">
                    <select name="company" id="companies-name">
                        <option value="">Select Company</option>
                        <?php
                        if (isset($companies) && count($companies) > 0) {
                            foreach ($companies as $company) {
                                ?>
                                <option value="{{$company->id}}">{{$company->name}}</option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="ti-select-wrap">
                    <select name="car" id="company-cars" >
                        <option value="">Select Car</option>
                    </select>
                </div>
                <div class="ti-select-wrap">
                    <select name="model" id="car-models">
                        <option value="">Select Model</option>
                    </select>
                </div>
            </div>
            <div id="ti-home-part-name" class="ti-select-wrap">
                <div id="ti-home-engine" class="ti-select-wrap"></div>
                <div class="clearfix"></div>
                <div>
                    <input type="submit" value="Go" id="tihomesubmit">
                </div>
            </div>
            {{ html()->form()->close() }}
        </div>
    </div>
</div><!--row-->
<div class="main-owl">
    <div class="subtitle subtitle_header">POPULAR CAR MAKERS </div>
    <div class="owl-carousel owl-theme " style="float: left; display: block !important;">
        <div class="outer-img">
            <a href="{{url('search?data=Mahindra')}}">
                <img src="{{asset('img/frontend/featured_brands/Mahindra_&_Mahindra_555eb99f19a11.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Maruti')}}">
                <img src="{{asset('img/frontend/featured_brands/Maruti-Suzuki-Logo_555db048a0f5f.jpg')}}" class="maruti">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Hyundai')}}">
                <img src="{{asset('img/frontend/featured_brands/a4d5424.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Toyota')}}">
                <img src="{{asset('img/frontend/featured_brands/Toyota-logo_5563f9c56193f.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Honda')}}">
                <img src="{{asset('img/frontend/featured_brands/Honda_logo_web_5551e8f16ed6f.gif')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Nissan')}}">
                <img src="{{asset('img/frontend/featured_brands/nissan-4_55802014bfcb4.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Renault')}}">
                <img src="{{asset('img/frontend/featured_brands/renault-auto_5570278d1c590.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=BMW')}}">
                <img src="{{asset('img/frontend/featured_brands/bmw-logo_553f2a013254e.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Skoda')}}">
                <img src="{{asset('img/frontend/featured_brands/Skoda-Logo_web_556409dbcca1a.jpg')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Ford')}}">
                <img src="{{asset('img/frontend/featured_brands/Ford_logo_web_554b0337bff4a.gif')}}">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Chevrolet')}}">
                <img src="{{asset('img/frontend/featured_brands/Chevrolet_logo_web_55474de373e0e.gif')}}" class="chev">
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Volkswagen')}}">
                <img src="{{asset('img/frontend/featured_brands/VW-Logo-web_55631ab4ef91b.jpg')}}">.
            </a>
        </div>
        <div class="outer-img">
            <a href="{{url('search?data=Tata')}}">
                <img src="{{asset('img/frontend/featured_brands/tata-web_556598fea81a3.jpg')}}">
            </a>
        </div>
    </div>
</div>

<div class="list_block opacity">
    <div class="subtitle subtitle_header">replacement parts</div>
    <ul class="clear_fix grayscale_block">
        <li>
            <a href="{{url('search?category=1') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/de0a128.jpg')}}" alt="Maintenance / Service Parts">
                <span>Maintenance / Service Parts</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=2') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b070b124eb2.jpg')}}" alt="Brakes">
                <span>Brakes</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=3') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b0c09a56b73.jpg')}}" alt="Suspension">
                <span>Suspension</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=4') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/8bd539c.jpg')}}" alt="Body">
                <span>Body</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=5') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/8aa08fe.jpg')}}" alt="Accessories">
                <span>Accessories</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=6') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/3d7c859.jpg')}}" alt="Electric Components &amp; Lights">
                <span>Electric Components &amp; Lights</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=7') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1460d4e.jpg')}}" alt="Engine">
                <span>Engine</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=8') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/9ecc479.jpg')}}" alt="Trims">
                <span>Trims</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=9') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/26d5ee9.jpg')}}" alt="Interior">
                <span>Interior</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=10') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/bd7e146.jpg')}}" alt="Steering">
                <span>Steering</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=11') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/bd681f2.jpg')}}" alt="Transmission">
                <span>Transmission</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=12') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b0bb2f04d2c.jpg')}}" alt="Fuel Supply System">
                <span>Fuel Supply System</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=13') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b0a5fe691fb.jpg')}}" alt="Air Conditioning / Heater">
                <span>Air Conditioning / Heater</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=14') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b0ac9847d85.jpg')}}" alt="Wheel Drive">
                <span>Wheel Drive</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=15') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_55b0b37b73d74.jpg')}}" alt="Exhaust System">
                <span>Exhaust System</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=16') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/1_57026956e4aa8.jpg')}}" alt="Universal">
                <span>Universal</span>
            </a>
        </li>
        <li>
            <a href="{{url('search?category=17') }}">
                <img class="grayscale" src="{{asset('img/frontend/categories/8146686.jpg')}}" alt="Lubricants">
                <span>Lubricants</span>
            </a>
        </li>
    </ul>
</div>
@endsection

@push('after-scripts')
<script>
    $(".owl-carousel").owlCarousel({
        responsiveClass: true,
        autoHeight: false,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 5,
                nav: true,
            }
        }
    });
</script>
@endpush