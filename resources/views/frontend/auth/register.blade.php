@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

<style>
    .card {
        margin-bottom: 70px;
    }
    .card-heading{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 0;
        padding-left: 20px;
    }
    .login-row{
        padding: 0 15px;
    }
    .login-row .col{
        border-top: 1px solid rgba(0, 0, 0, 0.125);
        padding-top: 10px;
        text-align: center;
        margin-top: 20px;
    }
    .login-row p{
        font-size: 15px;
    }
    .login-row a{
        font-size: 15px;
    }
    
</style>
@section('content')
<div class="row justify-content-center align-items-center register-box">
    <div class="col col-sm-8 align-self-center">
        <div class="card">
            <h2 class="card-heading">
                @lang('labels.frontend.auth.register_box_title')
            </h2>

            <div class="card-body">
                {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->text('first_name')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.first_name'))
                                        ->attribute('maxlength', 191) }}
                        </div><!--col-->
                    </div><!--row-->

                    <div class="col-12 col-md-6">
                        <div class="form-group">
                            {{ html()->text('last_name')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.last_name'))
                                        ->attribute('maxlength', 191) }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('phone_number')
                                        ->class('form-control')
                                        ->placeholder('Phone Number')
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->password('password_confirmation')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                @if(config('access.captcha.registration'))
                <div class="row">
                    <div class="col">
                        {!! Captcha::display() !!}
                        {{ html()->hidden('captcha_status', 'true') }}
                    </div><!--col-->
                </div><!--row-->
                @endif

                <div class="row">
                    <div class="col">
                        <div class="form-group mb-0 clearfix">
                            <button class="btn btn-sm pull-right" style="background-color: #16c7f6 !important; color:white;" type="submit">Register</button>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                <div class="row login-row">
                    <div class="col">
                        <div class="form-group clearfix">
                            <p>Already have a account. 
                                <a href="{{ route('frontend.auth.login') }}" class="">Login</a>
                            </p>
                        </div><!--form-group-->
                    </div>
                </div>
                {{ html()->form()->close() }}

                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            {!! $socialiteLinks !!}
                        </div>
                    </div><!--/ .col -->
                </div><!-- / .row -->

            </div><!-- card-body -->
        </div><!-- card -->
    </div><!-- col-md-8 -->
</div><!-- row -->
@endsection

@push('after-scripts')
@if(config('access.captcha.registration'))
{!! Captcha::script() !!}
@endif
@endpush
