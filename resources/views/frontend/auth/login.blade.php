@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

<style>
    .card {
        margin-bottom: 70px;
    }
    .card-heading{
        font: bold 21px/24px Arial;
        color: #000;
        margin-bottom: 0;
        padding-left: 20px;
    }
    .login-row{
        padding: 0 15px;
    }
    .login-row .col{
        border-top: 1px solid rgba(0, 0, 0, 0.125);
        padding-top: 10px;
        text-align: center;
        margin-top: 5px;
        padding-left: 0;
        padding-right: 0;
    }
    .login-row p{
        font-size: 15px;
    }
    .login-row a{
        font-size: 15px;
    }
</style>
@section('content')
<div class="row justify-content-center align-items-center login-box">
    <div class="col col-sm-8 align-self-center">
        <div class="card">
            <h2 class='card-heading'>
                @lang('labels.frontend.auth.login_box_title')
            </h2>

            <div class="card-body">
                {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->text('email')
                                        ->class('form-control')
                                        ->placeholder('Enter Email or Number')
                                        ->attribute('maxlength', 191)
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            {{ html()->password('password')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.password'))
                                        ->required() }}
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <div class="checkbox">
                                {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                            </div>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->


                <div class="row">

                    <div class="col">
                        <div class="form-group clearfix">
                            <button class="btn btn-sm pull-right" style="background-color: #16c7f6 !important; color:white;" type="submit">Login</button>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->

                <div class="row login-row">
                    <div class="col">
                        <div class="form-group text-left clearfix">
                            <p>Don't have account?
                                <a href="{{ route('frontend.auth.register') }}" class="">Register</a>
                            </p>
                        </div><!--form-group-->
                    </div><!--col-->
                    <div class="col">
                        <div class="form-group text-right">
                            <a href="{{ route('frontend.auth.password.reset') }}">@lang('labels.frontend.passwords.forgot_password')</a>
                        </div><!--form-group-->
                    </div><!--col-->
                </div><!--row-->
                {{ html()->form()->close() }}

                <div class="row">
                    <div class="col">
                        <div class="text-center">
                            {!! $socialiteLinks !!}
                        </div>
                    </div><!--col-->
                </div><!--row-->
            </div><!--card body-->
        </div><!--card-->
    </div><!-- col-md-8 -->
</div><!-- row -->
@endsection
