<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
    @else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        @endlangrtl
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <title>@yield('title', app_name())</title>
            <meta name="description" content="@yield('meta_description', '')">
            <meta name="author" content="@yield('meta_author', '')">
            @yield('meta')


            @stack('before-styles')

            <!-- Check if the language is set to RTL, so apply the RTL layouts -->
            <!-- Otherwise apply the normal LTR layouts -->
            {{ style(mix('css/frontend.css')) }}
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

            @stack('after-styles')
        </head>
        <body>
            <div id="app">
                <div class="container-fluid nav-main">
                    <div class="col">
                        @include('includes.partials.logged-in-as')
                        @include('frontend.includes.nav')

                        <!--<div class="container">-->
                        @include('includes.partials.messages')
                        @yield('content')
                        @include('frontend.includes.footer')
                    </div><!-- container -->
                </div>
            </div><!-- #app -->

            <!-- Scripts -->
            @stack('before-scripts')
            {!! script(mix('js/manifest.js')) !!}
            {!! script(mix('js/vendor.js')) !!}
            {!! script(mix('js/frontend.js')) !!}
            <script type="text/javascript" src="{{asset('js/common.js')}}"></script>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
            <script type="text/javascript" src="{{asset('js/owlcarousel/owl.carousel.min.js')}}"></script>
            <script type="text/javascript">
            var addToCartUrl = "{{route('frontend.addToCart')}}";
            </script>
            @stack('after-scripts')

            @include('includes.partials.ga')
        </body>
    </html>
