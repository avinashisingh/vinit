<footer>
    <div class="row_span clear_fix">
        <div class="legal-info">
            <span>&copy <?php echo date('Y'); ?> Vinit Automobile. All rights reserved.</span>
        </div>
        <div class=" text-center text-lg-right my-auto">
            <ul class="list-inline mb-2">
                <li class="list-inline-item">
                    <a href="{{ route('frontend.index') }}">Home</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('frontend.aboutUs') }}">About</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('frontend.contact') }}">Contact</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('frontend.termsOfUse') }}">Terms of Use</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('frontend.privacyPolicy') }}">Privacy Policy</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('frontend.returnPolicy') }}">Return Policy</a>
                </li>
            </ul>
        </div>
        <div class="footer-contacts">
            <div class="col-sm-6 left-side">
                <h3>Our Address</h3>
                <p>Shop no. 4/2 <br>
                    Vinit Automobile near Jawala Mandir <br>
                    Sector 45B <br>
                    Burail, Chandigarh</p>
            </div>
            <div class="col-sm-6 right-side">
                <h3>Contact Us</h3>
                <p>+91-8727852806</p>
            </div>
        </div>
    </div>
</footer>