<link href="{{ asset('css/frontend/header.css') }}" rel="stylesheet">

<div id="header">
    <div class="row_span clear_fix">
        <nav class="navbar navbar-expand-lg navbar-light" id="nav-top">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerMain" aria-controls="navbarTogglerMain" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarTogglerMain">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li>
                        <a href="{{ route('frontend.index') }}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.aboutUs') }}">About Us</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.contact') }}">Contact us</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.termsOfUse') }}">Terms of Use</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.privacyPolicy') }}">Privacy Policy</a>
                    </li>
                    <li>
                        <a href="{{ route('frontend.returnPolicy') }}">Return Policy</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="fl_right clear_fix" id="login-signup">
            @if(Auth::user())
            <div class="logged_in fl_right">
                <div class="inline_block">
                    <a id="logged-in" class="loggedin" href="javascript:void(0);">Hi, {{Auth::user()->first_name}}</a>
                </div>
                <div class="inline_block">
                    <a id="" class="loggedin" href="{{ route('frontend.user.myOrders') }}">My Orders</a>
                </div>
                <div class="inline_block">
                    <a id="my-account" class="loggedin" href="{{ route('frontend.user.account') }}">My Account</a>
                </div>
                <div class="inline_block">
                    <a id="logout" class="loggedin" href="{{ route('frontend.auth.logout') }}">Logout</a>
                </div>
            </div>
            @else
            <div class="login fl_right">
                <div class="inline_block">
                    <a id="signup" class="popup_toggle" href="javascript:void(0);">Sign up</a>
                    <div class="popup" id="mySignup">
                        <div class="registration_block">
                            <div class="name">Registration</div>
                            {{ html()->form('POST', route('frontend.auth.register.post'))->open() }}
                            <div class="form-group">
                                {{ html()->text('first_name')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.first_name'))
                                                ->attribute('maxlength', 191) }}
                            </div><!--col-->

                            <div class="form-group">
                                {{ html()->text('last_name')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.last_name'))
                                                ->attribute('maxlength', 191) }}
                            </div><!--form-group-->

                            <div class="form-group">
                                {{ html()->email('email')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.email'))
                                                ->attribute('maxlength', 191)
                                                ->required() }}
                            </div><!--form-group-->

                            <div class="form-group">
                                <input type="number" class="form-control" name="phone_number" id="phone_number" placeholder="Phone Number">
                            </div><!--form-group-->

                            <div class="form-group">
                                {{ html()->password('password')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.password'))
                                                ->required() }}
                            </div><!--form-group-->

                            <div class="form-group">
                                {{ html()->password('password_confirmation')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                                ->required() }}
                            </div><!--form-group-->

                            <div class="form-group mb-0 clearfix">
                                <button class="btn btn-sm pull-right" style="background-color: #16c7f6 !important; color:white;" type="submit">Register</button>
                            </div><!--form-group-->
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
                <div class="inline_block">
                    <a id="login" class="popup_toggle" href="javascript: void(0);">Log in</a>
                    <div class="popup" id="myLogin">
                        <div class="login_block">
                            <div class="name">Login</div>
                            {{ html()->form('POST', route('frontend.auth.login.post'))->open() }}
                            <div class="form-group">
                                {{ html()->text('email')
                                                ->class('form-control')
                                                ->placeholder('Enter Email or Number')
                                                ->attribute('maxlength', 191)
                                                ->required() }}
                            </div><!--form-group-->
                            <div class="form-group">
                                {{ html()->password('password')
                                                ->class('form-control')
                                                ->placeholder(__('validation.attributes.frontend.password'))
                                                ->required() }}
                            </div><!--form-group-->
                            <div class="form-group">
                                <div class="checkbox">
                                    {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                                </div>
                            </div><!--form-group-->
                            <div class="form-group clearfix">
                                <button class="btn btn-sm pull-right" style="background-color: #16c7f6 !important; color:white;" type="submit">Login</button>
                            </div><!--form-group-->
                            <div class="form-group text-right">
                                <a href="javascript: void(0);" class="recover_password">@lang('labels.frontend.passwords.forgot_password')</a>
                            </div><!--form-group-->
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
                <div class="inline_block">
                    <a id="recover" class="popup_toggle" href="javascript: void(0);" style="display: none;">Recover</a>
                    <div class="popup" id="recover-popup">
                        <div class="recover_block">
                            <div class="name">Recover Password</div>
                            {{ html()->form('POST', route('frontend.auth.password.email.post'))->open() }}
                            {{ html()->email('email')
                                        ->class('form-control')
                                        ->placeholder(__('validation.attributes.frontend.email'))
                                        ->attribute('maxlength', 191)
                                        ->required()
                                        ->autofocus() }}
                            <div class="row recover_row">
                                {{ form_submit('Recover now') }}
                            </div>
                            {{ html()->form()->close() }}
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>

    <div class="bottom table">
        <div class="logo">
            <a href="{{route('frontend.index')}}">
                <img src="{{asset('img/frontend/vinit-logo.png') }}" alt="Vinit Automobile" class="">
            </a>
        </div>
        <div class="search">
            <div id="search_block" class="search_block">
                {{ html()->form('GET', route('frontend.search'))->open(), ['class'=>'reds'] }}
                <div class="table">
                    <div class="input_cell">
                        <input class="search_input" name="data" type="text" id="query" value="" placeholder="Enter a Company Name or Car Name or Product Name">
                    </div>
                    <div class="submit_cell">
                        <input class="btn button submit_btn" id="submit" value="" type="submit">
                        <i class='fa fa-search'></i>
                    </div>
                </div>
                {{ html()->form()->close() }}
            </div>
        </div>
        <div class="header-buttons">
            <form action="" method="get" class="text-center">
                <a href="{{route('frontend.showCart')}}" class="header-button" type="submit">
                    <div class="header-buttons--counter ">{{Session::get('cartCount')??"0"}}</div>
                    <div class="header-buttons--icon icon-cart"></div>
                    <div class="header-buttons--label">CART</div>
                </a>
            </form>
        </div>
    </div>
</div>